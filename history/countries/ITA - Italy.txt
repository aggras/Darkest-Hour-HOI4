﻿#########################################################################
# Italy - 1933
#########################################################################
1933.1.1 = {
	capital = 2
	set_stability = 0.6
	set_war_support = 0.7
	oob = "ITA_1933"
	set_research_slots = 3
	set_convoys = 200
	add_to_variable = { money = 450 }
	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1900 = 1
		Small_Arms_1916 = 1
		Uniform_1914 = 1
		Uniform_1918 = 1
		Support_Weapons_1914 = 1
		Support_Weapons_1918 = 1
		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		### Support Units
		tech_support = 1
		tech_mountaineers = 1
		### Artillery Tech
		Artillery_1910 = 1
        Anti_Aircraft_Gun_1914 = 1
		Artillery_Range_Finding_and_Surveying_Tools = 1
		Infantry_Support_Gun_1914 = 1
        Artillery_1916 = 1
        High_Explosive_Shells = 1
		Split_Trail_Carriage = 1
		Motor_Compatible_Carriage = 1
		### Armour Tech
		Motorized_1916 = 1
		Armored_Car_1911 = 1
        Armored_Car_1916 = 1
        Armored_Car_1926 = 1
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
		Light_Tank_1917 = 1
		Light_Tank_1919 = 1
		Light_Tank_1926 = 1
		### Air Tech
		Unarmed_Recon_1910 = 1
        Fighter_1914 = 1
        Fighter_1916 = 1
        Fighter_1918 = 1
        Fighter_1924 = 1
		Fighter_Bomber_1916 = 1
        Fighter_Bomber_1918 = 1
        Fighter_Bomber_1924 = 1
		Tactical_Bomber_1910 = 1
        Tactical_Bomber_1914 = 1
        Tactical_Bomber_1916 = 1
        Tactical_Bomber_1918 = 1
        Tactical_Bomber_1925 = 1
		Naval_Bomber_1918 = 1
        Naval_Bomber_1925 = 1
		Strategic_Bomber_1916 = 1
		### Naval stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CL_1922 = 1
		CL_1933 = 1
		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		CA_1922 = 1
		CA_1933 = 1
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BB_1906 = 1
		BB_1912 = 1
		BB_1916 = 1
		BB_1922 = 1
		CV_1912 = 1
		transport = 1
		fleet_in_being = 1
		### Industry
		Industrial_Management = 1
		Factory_Electrification = 1
		Facilities_Design = 1
		Moving_Assembly_Line = 1
		Skilled_Workforce = 1
		Dispersed_Industry = 1
		Basic_Construction_Machines = 1
		Novel_Drills = 1
		Contemporary_Construction_Standards = 1
		Advanced_Drilling_Method = 1
		Construction_Site_Management = 1
		Motorized_Plowing = 1
		Farm_Tractors = 1
		Scientific_Agricultural_Processes = 1
		Mechanized_Agriculture = 1
		Rudimentary_Oil_Refining = 1
		### Land Doctrines
		Twentieth_Century_Warfare = 1
		Leading_by_Order = 1
		Strongpoint = 1
        Mass_Charge = 1
        Static_Defence = 1
        Counterattack = 1
		Defence_In_Depth = 1
		Separate_Arms_Force = 1
        Combined_Arms_Effort = 1
		Specialized_Branch_Tactical_Experience = 1
		Beyond_Trench_Warfare = 1
		### Engineering
		computing_1 = 1
		computing_2 = 1
		computing_3 = 1
		computing_4 = 1
		decryption_encryption_1 = 1
		### Experimental Research
		rocketry_1 = 1
		nuclear_1 = 1
		### Support Companies
		tech_engineers = 1
		tech_assault_weapons_1 = 1
		tech_assault_weapons_2 = 1
		tech_assault_weapons_3 = 1
		tech_entrenchment_1 = 1
		tech_entrenchment_2 = 1
		tech_entrenchment_3 = 1
		tech_amphibious_1 = 1
		tech_amphibious_2 = 1
		tech_amphibious_3 = 1
		tech_recon = 1
		tech_recon2 = 1
		tech_recon3 = 1
		tech_recon4 = 1
		tech_military_police = 1
		tech_military_police2 = 1
		tech_military_police3 = 1
		tech_military_police4 = 1
		tech_signal_company = 1
		tech_signal_company2 = 1
		tech_field_hospital = 1
		tech_field_hospital_psychology_1 = 1
		tech_field_hospital_psychology_2 = 1
		tech_field_hospital_psychology_3 = 1
		tech_field_hospital_health_1 = 1
		tech_field_hospital_health_2 = 1
		tech_field_hospital_health_3 = 1
		tech_logistics_company = 1
		tech_logistics_company2 = 1
		tech_logistics_company3 = 1
		tech_logistics_company4 = 1
		tech_maintenance_company = 1
		tech_maintenance_company2 = 1
		tech_maintenance_company3 = 1
		tech_maintenance_company4 = 1
	}
	#######################
	# Politics
	#######################
	set_country_flag = language_italian
	set_politics = {
		ruling_party = fascist
		last_election = "1934.3.26"
		election_frequency = 60
		elections_allowed = no
	}
	set_popularities = {
		fascist = 52
		authoritarian = 5
		democratic = 33
		socialist = 1
		communist = 9
	}
	### Great Depression/Unemployment
	add_ideas = generic_unemployment_idea
	set_variable = { current_unemployment = 0.30 }
	clamp_variable = {
		var = current_unemployment
		min = 0
		max = 1
	}
	update_unemployment_modifier = yes
	add_ideas = {
		ITA_Birthplace_of_Fascism
		ITA_The_Corporatist_Experiment
		ITA_High_Command_Incompetence
		ITA_Regio_Esercito_Reorganization
		ITA_Overfunded_Regia_Marina
		ITA_Carta_del_Lavoro
		# GENERIC_London_Naval_Treaty
		# vittoria_mutilata
		limited_exports
		one_year_service
		mixed_economy
		legalistic_restrictions
		# Cabinet
		ITA_HoS_Victor_Emmanuel_III
		ITA_FM_Gian_Galeazzo_Ciano
		ITA_AM_Guido_Jung
		ITA_MoS_Guido_BuffariniGuidi
		ITA_HoI_Cesare_Ame
		# Military Staff
		ITA_CoStaff_Francesco_Grazioli
		ITA_CoArmy_Alberto_Pariani
		ITA_CoNavy_Domenico_Cavagnari
		ITA_CoAir_Giuseppe_Valle
	}
	create_intelligence_agency = {
		name = "Servizio Informazioni Militare"
		icon = "GFX_intelligence_agency_logo_ita"
	}
	#######################
	# Diplomacy
	#######################
	if = {
		limit = {
			has_start_date > 1935.12.31
			has_start_date < 1936.1.2
		}
		declare_war_on = {
			target = ETH
			type = annex_everything
		}
	}
	give_guarantee = ALB
	give_guarantee = AUS
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Benito Mussolini"
		desc = "Benito_Mussolini_desc"
		picture = GFX_P_F_Benito_Mussolini
		expire = "1965.1.1"
		ideology = fascism
		traits = {
			POSITION_ITA_Duce
			SUBIDEOLOGY_Fascism
			HoG_Imperialist_Buffoon
		}
	}
	# Authoritarian
	create_country_leader = {
		name = "Vittorio Emanuele III"
		desc = ""
		picture = GFX_P_A_Victor_Emmanuel_III
		expire = "1965.1.1"
		ideology = monarchism
		traits = {
			POSITION_King
			SUBIDEOLOGY_Monarchism
			HoS_Weary_Stiff_Neck
		}
	}
	# democratic
	create_country_leader = {
		name = "Alcide de Gasperi"
		desc = ""
		picture = GFX_P_D_Alcide_de_Gasperi
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = {
			POSITION_President
			SUBIDEOLOGY_Social_Conservatism
			HoG_Backroom_Backstabber
		}
	}
	# Socialism
	create_country_leader = {
		name = "Pietro Nenni"
		desc = ""
		picture = GFX_P_S_Pietro_Nenni
		expire = "1965.1.1"
		ideology = socialism
		traits = {
			POSITION_President
			SUBIDEOLOGY_Socialism
			FM_The_Cloak_N_Dagger_Schemer
		}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Palmiro Togliatti"
		desc = ""
		picture = GFX_P_C_Palmiro_Togliatti
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {
			POSITION_General_Secretary
			SUBIDEOLOGY_Marxism_Leninism
			HoS_PowerHungry_Demagogue
		}
	}
	#######################
	# Generals
	#######################
	create_field_marshal = {
		name = "Pietro Badoglio"
	    id = 134000
		GFX = GFX_M_Pietro_Badoglio
		traits = {
			defensive_doctrine
			old_guard
			politically_connected
			inflexible_strategist
		}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_field_marshal = {
		name = "Emilio De Bono"
	    id = 134001
		GFX = GFX_M_Emilio_De_Bono
		traits = {
			defensive_doctrine
			old_guard
			politically_connected
			trait_cautious
		}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Ugo Cavallero"
	    id = 134002
		GFX = GFX_M_Ugo_Cavallero
		traits = {
			old_guard
			trait_cautious
			career_officer
		}
		skill = 2
		attack_skill = 1
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Annibale Bergonzoli"
	    id = 134003
		GFX = GFX_M_Annibale_Bergonzoli
		traits = {
			politically_connected
			trait_reckless
			infantry_officer
			harsh_leader
		}
		skill = 2
		attack_skill = 1
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Antero Leonardo Canale"
	    id = 134004
		GFX = GFX_M_Antero_Leonardo_Canale
		traits = {

		}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Carlo Geloso"
	    id = 134005
		GFX = GFX_M_Carlo_Geloso
		traits = {

		}
		skill = 2
		attack_skill = 1
		defense_skill = 3
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Gabriele Nasci"
	    id = 134006
		GFX = GFX_M_Gabriele_Nasci
		traits = {
			trait_mountaineer
			winter_specialist
		}
		skill = 2
		attack_skill = 2
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Mario Roatta"
	    id = 134007
		GFX = GFX_M_Mario_Roatta
		traits = {
			politically_connected
			career_officer
			infantry_officer
			harsh_leader
		}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Giovanni Messe"
	    id = 134008
		GFX = GFX_M_Giovanni_Messe
		traits = {
			armor_officer
			war_hero
		}
		skill = 4
		attack_skill = 4
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 4
	}
	create_corps_commander = {
		name = "Italo Balbo"
	    id = 134009
		GFX = GFX_M_Italo_Balbo
		traits = {
			career_officer
			politically_connected
		}
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
        name = "Alessandro Pirzio Biroli"
	    id = 134010
        GFX = GFX_M_Alessandro_Pirzio_Biroli
        traits = {

        }
        skill = 1
        attack_skill = 1
        defense_skill = 1
        planning_skill = 1
        logistics_skill = 1
	}
	create_corps_commander = {
        name = "Ettore Bastico"
	    id = 134011
        GFX = GFX_M_Ettore_Bastico
        traits = {

        }
        skill = 1
        attack_skill = 1
        defense_skill = 1
        planning_skill = 1
        logistics_skill = 1
	}
	create_corps_commander = {
        name = "Carlo Bergamini"
	    id = 134012
        GFX = GFX_M_Carlo_Bergamini
        traits = {

        }
        skill = 1
        attack_skill = 1
        defense_skill = 1
        planning_skill = 1
        logistics_skill = 1
	}
	create_field_marshal = {
		name = "Rodolfo Graziani"
	    id = 134013
		GFX = GFX_M_Rodolfo_Graziani
		traits = {
			offensive_doctrine
			politically_connected
			trait_cautious
		}
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
	}
	#######################
	# Admirals
	#######################
	create_navy_leader = {
		name = "Inigo Campioni"
		picture = GFX_Portrait_Italy_Inigo_Campioni
		traits = {
			superior_tactician
			spotter
		}
		skill = 2
	}
	create_navy_leader = {
		name = "Alberto Da Zara"
		picture = GFX_Portrait_Italy_Alberto_Da_Zara
		traits = {
			superior_tactician
		}
		skill = 3
	}
	create_navy_leader = {
		name = "Giuseppe Fioravanzo"
		GFX = GFX_M_Giuseppe_Fioravanzo
		traits = {
			superior_tactician
		}
		skill = 3
	}
	create_navy_leader = {
		name = "Carlo Bergamini"
		GFX = GFX_M_Carlo_Bergamini
		traits = {
			superior_tactician
		}
		skill = 2
	}
	create_navy_leader = {
		name = "Angelo Iachino"
		GFX = GFX_M_Angelo_Iachano
		traits = {
			superior_tactician
			spotter
		}
		skill = 2
	}
}

#########################################################################
# Italy - 1936
#########################################################################
1936.1.1 = {
	oob = "ITA_1936"
	#######################
	# Research
	#######################
	set_technology = {
		ITA_MVSN = 1
		Small_Arms_1935 = 1
		CAS_1936 = 1
		Fighter_1933 = 1
	}
	#######################
	# Diplomacy
	#######################
	### Italo-Soviet Pact
	diplomatic_relation = {
		country = SOV
		relation = non_aggression_pact
		active = yes
	}
	#######################
	# Politics
	#######################
	add_ideas = {
		limited_exports
		one_year_service
		partial_economic_mobilisation
		mixed_economy
		legalistic_restrictions
	}
}

#########################################################################
# Italy - 1944
#########################################################################
1944.6.20 = {
	capital = 2
	oob = "ITA_1944"
	drop_cosmetic_tag = yes
	set_politics = {
		ruling_party = authoritarian
		last_election = "1934.3.26"
		election_frequency = 60
		elections_allowed = no
	}
	set_popularities = {
		democratic = 44
		authoritarian = 56
		fascist = 0
		socialist = 0
		communist = 0
	}
	add_ideas = {
		war_economy
		service_by_requirement
		closed_economy
	}
}

#########################################################################
# Italy - 1950
#########################################################################
1950.1.1 = {
	capital = 2
	set_politics = {
		ruling_party = democratic
		last_election = "1934.3.26"
		election_frequency = 60
		elections_allowed = no
	}
	set_popularities = {
		democratic = 56
		authoritarian = 44
		fascist = 0
		socialist = 0
		communist = 0
	}
}
