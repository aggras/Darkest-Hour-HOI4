division_template = {
	name = "Corpo Truppe Voluntarie"
	is_locked = yes
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 } 
	}
	
	priority = 2
}
units = {
	division = {
		name = "1a Divisione di Fanteria 'Dio lo Vuole'"
		location = 4195
		division_template = "Corpo Truppe Voluntarie"
		start_experience_factor = 0.3
	}
	division = {
		name = "2a Divisione di Fanteria 'Fiamme Nere'"
		location = 4195
		division_template = "Corpo Truppe Voluntarie"
		start_experience_factor = 0.3
	}
	division = {
		name = "Missione Armate Italiane"
		location = 4195
		division_template = "Corpo Truppe Voluntarie"
		start_experience_factor = 0.3
	}
}