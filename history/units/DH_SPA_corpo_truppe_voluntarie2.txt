division_template = {
	name = "Corpo Truppe Volontarie"
	is_locked = yes
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 } 
	}
	
	priority = 2
}
units = {
	division = {
		name = "4a Divisione di Fanteria 'Dio lo Vuole'"
		location = 4195
		division_template = "Corpo Truppe Volontarie"
		start_experience_factor = 0.3
	}
	division = {
		name = "Divisione XXIII Marzo"
		location = 4195
		division_template = "Corpo Truppe Volontarie"
		start_experience_factor = 0.3
	}
	division = {
		name = "3a Divisione di Fanteria 'Penne Nere'"
		location = 4195
		division_template = "Corpo Truppe Volontarie"
		start_experience_factor = 0.3
	}
}