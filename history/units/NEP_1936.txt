﻿division_template = {
	name = "Paidala Dibhijana"		# Represents groupings of independent Nepalese battalions	

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 0 y = 3 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 1 y = 3 }
	}
}

units = {
	##### Sahi Nepali Sena #####
	division = {
		name = "Royal Nepalese Lifeguard Division"
		location = 4987 # Kathmandu
		division_template = "Paidala Dibhijana"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		name = "Royal Household Division"
		location = 4987 # Kathmandu
		division_template = "Paidala Dibhijana"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
}

### No notable air forces ###

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "NEP"
		}
		requested_factories = 1
		progress = 0.89
		efficiency = 100
	}
}