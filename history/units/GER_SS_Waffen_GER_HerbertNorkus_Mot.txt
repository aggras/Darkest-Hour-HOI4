﻿units = {
	division= {
		name = "SS-Division (mot.) „Herbert Norkus“"
		location = 495 # Actually trained at Leopoldsburg, Belgium
		division_template = "SS-Infanterie-Division (mot.)"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}