﻿units = {
	division= {
		name = "Waffen-Grenadier-Division der SS „Herakles“ (griechische Nr. 2)"
		location = 6325 # Lüneburg Heath
		division_template = "SS-Infanterie-Division"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}