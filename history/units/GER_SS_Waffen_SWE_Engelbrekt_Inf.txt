﻿units = {
	division= {
		name = "SS-Freiwilligen-Grenadier-Division „Engelbrekt“"
		location = 9496 # Kurmark
		division_template = "SS-Infanterie-Division"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}