﻿units = {
	division= {
		name = "SS-Freiwilligen-Gebirgs-Division „Prinz Eugen“"
		location = 3614 # Pančevo
		division_template = "SS-Gebirgsjäger-Division"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}