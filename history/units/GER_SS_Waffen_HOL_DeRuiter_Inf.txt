﻿units = {
	division= {
		name = "SS-Freiwilligen-Grenadier-Division „De Ruiter“ (niederländische Nr. 3)"
		location = 6241 # Apeldoorn
		division_template = "SS-Infanterie-Division"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}