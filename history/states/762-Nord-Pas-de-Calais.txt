
state={
	id=762
	name="STATE_762"
	resources={
		steel=56.000
		aluminium=11.000
	}

	history={
		owner = FRA
		victory_points = {
			11548 3 
		}
		victory_points = {
			6504 1
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 1
			11548 = {
				naval_base = 5

			}
			11606 = {
				bunker = 1

			}

		}
		add_core_of = FRA
		1936.1.1 = {
			buildings = {
				11606 = {
					bunker = 2

				}
				575 = {
					bunker = 2

				}
				
			}
			
		}
		1941.6.22 = {
			controller = GER
			add_core_of = VIC

		}
		1945.1.1 = {
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		3536 6504 11548 11606 
	}
	manpower=2006276
	buildings_max_level_factor=1.000
	state_category=town
}
