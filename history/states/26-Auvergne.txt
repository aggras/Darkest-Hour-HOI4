
state={
	id=26
	name="STATE_26"

	history={
		owner = FRA
		victory_points = {
			9607 3 
		}
		victory_points = {
			590 2 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 1

		}
		add_core_of = FRA
		1941.6.22 = {
			owner = VIC
			controller = VIC
			add_core_of = VIC

		}
		1943.7.26 = {
			owner = FRA
			controller = GER

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		590 769 3594 3754 6644 6773 6789 9593 9607 9748 11576 
	}
	manpower=877800
	buildings_max_level_factor=1.000
	state_category=town
}
