
state={
	id=619
	name="STATE_619"
	resources={
		aluminium=2.000
		chromium=2.000
	}

	history={
		owner = SIK
		victory_points = {
			5016 2
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		add_core_of = ETS
		1950.1.1 = {
			owner = PRC

		}

	}

	provinces={
		1766 2015 5016 5087 12119 14853 
	}
	manpower=1230000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
