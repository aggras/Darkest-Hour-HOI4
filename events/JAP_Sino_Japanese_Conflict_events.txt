######################################################
# Darkest Hour Events : Sino-Japanese Conflict
######################################################
add_namespace = JAP_CHI_Anti_Japanese_Army
add_namespace = JAP_CHI_Mongolian_Confederation
add_namespace = JAP_CHI_Mongol_Skirmish
add_namespace = JAP_CHI_Sino_Japanese_War
###########################
# Formation of the Anti-Japanese Army
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.1
	title = JAP_CHI_Anti_Japanese_Army.1.t
	desc = JAP_CHI_Anti_Japanese_Army.1.d
	picture = GFX_report_CHI_Feng_Yuxiang
	fire_only_once = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.1.A
		AJC = {
			transfer_state = 612
			transfer_state = 611
			hidden_effect = {
				load_oob = AJC_1936
			}
			set_cosmetic_tag = AJC_AJA
		}
		hidden_effect = {
			country_event = { id = JAP_CHI_Anti_Japanese_Army.2 days = 7 }
		}
	}
}
###########################
# Chahar Army Attacks Japan
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.2
	title = JAP_CHI_Anti_Japanese_Army.2.t
	desc = JAP_CHI_Anti_Japanese_Army.2.d
	picture = GFX_report_CHI_Anti_Japanese_Army
	fire_only_once = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.2.A
		transfer_state = 610
		611 = {
			set_border_war = yes
		}
		610 = {
			set_border_war = yes
		}
		start_border_war = {
			change_state_after_war = no
			attacker = {
				state = 611
				num_provinces = 2
				on_win = JAP_CHI_Anti_Japanese_Army.3
				on_lose = JAP_CHI_Anti_Japanese_Army.4
				on_cancel = JAP_CHI_Anti_Japanese_Army.4
			}
			defender = {
				state = 610
				num_provinces = 3
				on_win = JAP_CHI_Anti_Japanese_Army.4
				on_lose = JAP_CHI_Anti_Japanese_Army.3
				on_cancel = JAP_CHI_Anti_Japanese_Army.4
			}
		}
		hidden_effect = {
			set_border_war_data = {
				attacker = 611
				defender = 610
				attacker_modifier = 0.1
				defender_modifier = -0.1
			}
		}
	}
}
###########################
# Chahar Army Wins the Skirmish
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.3
	title = JAP_CHI_Anti_Japanese_Army.3.t
	desc = JAP_CHI_Anti_Japanese_Army.3.d
	picture = GFX_report_CHI_Chahar_Operation
	fire_only_once = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.3.A
		army_experience = 20
		# ALTERNATE HISTORY
		MAN = {
			transfer_state = 610
		}
		611 = { set_border_war = no }
		610 = { set_border_war = no }
	}
}
###########################
# Chahar Army Loses the Skirmish
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.4
	title = JAP_CHI_Anti_Japanese_Army.4.t
	desc = JAP_CHI_Anti_Japanese_Army.4.d
	picture = GFX_report_JAP_Battle_of_Rehe
	fire_only_once = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.4.A
		MAN = {
			transfer_state = 610
		}
		hidden_effect = {
			news_event = { id = JAP_CHI_Anti_Japanese_Army.5 days = 1 }
		}
		611 = { set_border_war = no }
		610 = { set_border_war = no }
	}
}
###########################
# Anti-Japanese Army Announced to be Disbanded
###########################
news_event = {
	id = JAP_CHI_Anti_Japanese_Army.5
	title = JAP_CHI_Anti_Japanese_Army.5.t
	desc = JAP_CHI_Anti_Japanese_Army.5.d
	picture = GFX_news_CHI_Anti_Japanese_Army
	major = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.5.A
		trigger = {
			NOT = { original_tag = AJC }
		}
		hidden_effect = {
			news_event = { id = JAP_CHI_Anti_Japanese_Army.6 days = 7 }
		}
	}
	option = {
		name = JAP_CHI_Anti_Japanese_Army.5.B
		trigger = {
			original_tag = AJC
		}
		hidden_effect = {
			add_ideas = AJC_Official_Disbandment
			hidden_effect = {
				news_event = { id = JAP_CHI_Anti_Japanese_Army.6 days = 7 }
			}
		}
	}
}
###########################
# The Anti-Japanese Bandit-Punishing Army
###########################
news_event = {
	id = JAP_CHI_Anti_Japanese_Army.6
	title = JAP_CHI_Anti_Japanese_Army.6.t
	desc = JAP_CHI_Anti_Japanese_Army.6.d
	picture = GFX_news_CHI_Anti_Japanese_Bandit_Punishing_Army
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.6.A
		trigger = {
			NOT = { original_tag = AJC }
		}
		hidden_effect = {
			news_event = { id = JAP_CHI_Anti_Japanese_Army.7 days = 15 }
		}
	}
	option = {
		name = JAP_CHI_Anti_Japanese_Army.6.B
		trigger = {
			original_tag = AJC
		}
		hidden_effect = {
			swap_ideas = {
				add_idea = AJC_A_New_Army
				remove_idea = AJC_Official_Disbandment
			}
			set_cosmetic_tag = AJC_BPA
			news_event = { id = JAP_CHI_Anti_Japanese_Army.7 days = 15 }
		}
	}
}
###########################
# Fang Zhenwu Occupies Gaoliying
###########################
news_event = {
	id = JAP_CHI_Anti_Japanese_Army.7
	title = JAP_CHI_Anti_Japanese_Army.7.t
	desc = JAP_CHI_Anti_Japanese_Army.7.d
	picture = GFX_news_CHI_Anti_Japanese_Army_2
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.7.A
		trigger = {
			NOT = { original_tag = AJC }
		}
	}
	option = {
		name = JAP_CHI_Anti_Japanese_Army.7.B
		trigger = {
			original_tag = AJC
		}
		JAP = {
			hidden_effect = {
				country_event = { id = JAP_CHI_Anti_Japanese_Army.8 days = 2 }
			}
		}
	}
}
###########################
# Issue a Warning to Fang Zhenwu
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.8
	title = JAP_CHI_Anti_Japanese_Army.8.t
	desc = JAP_CHI_Anti_Japanese_Army.8.d
	picture = GFX_report_CHI_Feng_Zhenwu
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.8.A
		AJC = {
			country_event = { id = JAP_CHI_Anti_Japanese_Army.9 days = 1 }
		}
	}
}
###########################
# Japan Issues us a Warning
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.9
	title = JAP_CHI_Anti_Japanese_Army.9.t
	desc = JAP_CHI_Anti_Japanese_Army.9.d
	picture = GFX_report_JAP_Japanese_Mechanized_Infantry
	is_triggered_only = yes
	# Never!
	option = {
		name = JAP_CHI_Anti_Japanese_Army.9.A
		ai_chance = {
			factor = 100
		}
		JAP = {
			country_event = { id = JAP_CHI_Anti_Japanese_Army.10 days = 1 }
		}
	}
	# We shall back down
	option = {
		name = JAP_CHI_Anti_Japanese_Army.9.B
		ai_chance = {
			factor = 0
		}
		# ALTERNATE HISTORY
	}
}
###########################
# Fang Does not Back Down
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.10
	title = JAP_CHI_Anti_Japanese_Army.10.t
	desc = JAP_CHI_Anti_Japanese_Army.10.d
	picture = GFX_report_CHI_Feng_Zhenwu
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.10.A
		JAP = {
			transfer_state = 610
		}
		start_border_war = {
			change_state_after_war = no
			attacker = {
				state = 611
				num_provinces = 2
				on_win = JAP_CHI_Anti_Japanese_Army.12
				on_lose = JAP_CHI_Anti_Japanese_Army.11
			}
			defender = {
				state = 610
				num_provinces = 3
				on_win = JAP_CHI_Anti_Japanese_Army.11
				on_lose = JAP_CHI_Anti_Japanese_Army.12
			}
		}
		611 = { set_border_war = yes }
		610 = { set_border_war = yes }
		hidden_effect = {
			set_border_war_data = {
				attacker = 611
				defender = 610
				attacker_modifier = 0.1
				defender_modifier = -0.1
			}
		}
	}
}
###########################
# Defeat of the Anti-Japanese Bandit-Punishing Army
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.11
	title = JAP_CHI_Anti_Japanese_Army.11.t
	desc = JAP_CHI_Anti_Japanese_Army.11.d
	picture = GFX_report_JAP_Japanese_Mechanized_Infantry
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.11.A
		AJC = {
			transfer_state = 622
			transfer_state = 756
			transfer_state = 744
			drop_cosmetic_tag = yes
		}
		SHX = {
			transfer_state = 612
			transfer_state = 611
			transfer_state = 827
		}
		MAN = {
			transfer_state = 610
		}
		CHI = {
			transfer_state = 608
			hidden_effect = {
				country_event = { id = JAP_CHI_Mongolian_Confederation.1 days = 42 }
			}
		}
		611 = { set_border_war = no }
		610 = { set_border_war = no }
	}
}
###########################
# We have been Defeated (Japanese)
###########################
country_event = {
	id = JAP_CHI_Anti_Japanese_Army.12
	title = JAP_CHI_Anti_Japanese_Army.12.t
	desc = JAP_CHI_Anti_Japanese_Army.12.d
	picture = GFX_report_JAP_Soldiers_in_Manchuria
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Anti_Japanese_Army.12.A
		611 = { set_border_war = no }
		610 = { set_border_war = no }
		MAN = {
			transfer_state = 610
		}
		# ALTERNATE HISTORY
	}
}
###########################
# The Confederation of Inner Mongolian States demand Autonomy (ROC)
###########################
country_event = {
	id = JAP_CHI_Mongolian_Confederation.1
	title = JAP_CHI_Mongolian_Confederation.1.t
	desc = JAP_CHI_Mongolian_Confederation.1.d
	picture = GFX_report_MEN_Yondonwangchug
	is_triggered_only = yes
	# We cannot risk war with Japan
	option = {
		name = JAP_CHI_Mongolian_Confederation.1.A
		ai_chance = {
			factor = 100
		}
		MEN = {
			transfer_state = 612
			transfer_state = 611
			hidden_effect = {
				load_oob = MEN_1936
			}
		}
		hidden_effect = {
			JAP = {
				end_puppet = MEN
				country_event = { id = JAP_CHI_Mongolian_Confederation.2 days = 515 }
			}
			MEN = {
				country_event = { id = JAP_CHI_Mongolian_Confederation.2 days = 515 }
			}
		}
	}
	# Inner-Mongolia belongs to China
	option = {
		name = JAP_CHI_Mongolian_Confederation.1.B
		ai_chance = {
			factor = 0
		}
	}
}
###########################
# Jiro Minami Announces his Support for Demchungdongrub (Japan and Inner Mongol State)
###########################
country_event = {
	id = JAP_CHI_Mongolian_Confederation.2
	title = JAP_CHI_Mongolian_Confederation.2.t
	desc = JAP_CHI_Mongolian_Confederation.2.d
	picture = GFX_report_JAP_Jiro_Minami
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongolian_Confederation.2.A
		612 = {
			add_state_modifier = {
				modifier = {
					local_manpower = -0.15
					state_production_speed_buildings_factor = -0.1
				}
			}
		}
		hidden_effect = {
			609 = {
				CONTROLLER = {
					if = {
						limit = {
							NOT = { original_tag = JAP }
							NOT = { is_puppet_of = JAP }
						}
						country_event = { id = JAP_CHI_Mongolian_Confederation.3 days = 205 }
					}
				}
			}
		}
	}
}
###########################
# He-Umezu Agreement (Controller of East Hebei State)
###########################
country_event = {
	id = JAP_CHI_Mongolian_Confederation.3
	title = JAP_CHI_Mongolian_Confederation.3.t
	desc = JAP_CHI_Mongolian_Confederation.3.d
	picture = GFX_report_JAP_Yoshijiro_Umezu
	is_triggered_only = yes
	# Better under China than the Kwantung army
	option = {
		name = JAP_CHI_Mongolian_Confederation.3.A
		ai_chance = {
			factor = 0
		}
		609 = {
			CONTROLLER = {
				if = {
					limit = {
						NOT = { original_tag = JAP }
						NOT = { is_puppet_of = JAP }
					}
					JAP = {
						create_wargoal = {
							type = puppet_wargoal_focus
							target = ROOT
						}
					}
				}
			}
		}
	}
	# The Kuomintang shall no longer rule us
	option = {
		name = JAP_CHI_Mongolian_Confederation.3.B
		ai_chance = {
			factor = 100
		}
		EHB = {
			transfer_state = 609
		}
		hidden_effect = {
			JAP = {
				country_event = { id = JAP_CHI_Mongolian_Confederation.4 days = 7 }
				set_autonomy = {
					target = EHB
					autonomy_state = AUTONOMY_Imperial_Protectorate
				}
			}
			CHI = {
				country_event = { id = JAP_CHI_Mongolian_Confederation.4 days = 7 }
				news_event = { id = JAP_CHI_Mongolian_Confederation.5 days = 8 }
			}
		}
	}
}
###########################
# The North Chahar Incident (China and Japan)
###########################
country_event = {
	id = JAP_CHI_Mongolian_Confederation.4
	title = JAP_CHI_Mongolian_Confederation.4.t
	desc = JAP_CHI_Mongolian_Confederation.4.d
	picture = GFX_report_CHI_Chahar_Operation
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongolian_Confederation.4.A
		trigger = {
			original_tag = JAP
		}
		612 = {
			add_state_modifier = {
				modifier = {
					local_manpower = 0.15
					state_production_speed_buildings_factor = 0.1
				}
			}
		}
	}
	#
	option = {
		name = JAP_CHI_Mongolian_Confederation.4.B
		trigger = {
			original_tag = CHI
		}
		HCP = {
			transfer_state = 608
			transfer_state = 614
		}
		614 = {
			set_demilitarized_zone = yes
		}
		608 = {
			set_demilitarized_zone = yes
		}
		CHI = {
			set_autonomy = {
				target = HCP
				autonomy_state = AUTONOMY_Satellite
			}
		}
	}
}
###########################
# Chin-Doihara Agreement (World)
###########################
news_event = {
	id = JAP_CHI_Mongolian_Confederation.5
	title = JAP_CHI_Mongolian_Confederation.5.t
	desc = JAP_CHI_Mongolian_Confederation.5.d
	picture = GFX_news_JAP_Chin_Doihara_Agreement
	major = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongolian_Confederation.5.A
	}
}
###########################
# Meeting Between Minami and Demchungdongrub (Japan and Inner Mongol State)
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.1
	title = JAP_CHI_Mongol_Skirmish.1.t
	desc = JAP_CHI_Mongol_Skirmish.1.d
	picture = GFX_report_MEN_Demchungdongrub
	fire_only_once = yes
	trigger = {
		OR = {
			original_tag = JAP
			original_tag = MEN
		}
		date > 1935.4.1
		has_start_date < 1935.4.1
	}
	# Aid the Inner Mongol State
	option = {
		name = JAP_CHI_Mongol_Skirmish.1.A
		trigger = {
			original_tag = JAP
		}
	}
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.1.B
		trigger = {
			original_tag = MEN
		}
		country_event = { id = JAP_CHI_Mongol_Skirmish.2 days = 2 }
	}
}
###########################
# Minami Sends Men to Demchungdongrub (Inner Mongol State)
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.2
	title = JAP_CHI_Mongol_Skirmish.2.t
	desc = JAP_CHI_Mongol_Skirmish.2.d
	picture = GFX_report_JAP_Jiro_Minami
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.2.A
		JAP = {
			puppet = MEN
		}
		MEN = {
			transfer_state = 826
			set_cosmetic_tag = MEN_MILITARY
		}
		hidden_effect = {
			news_event = { id = JAP_CHI_Mongol_Skirmish.3 days = 1 }
		}
	}
}
###########################
# Foundation of the Mongol Military Government (Every Country)
###########################
news_event = {
	id = JAP_CHI_Mongol_Skirmish.3
	title = JAP_CHI_Mongol_Skirmish.3.t
	desc = JAP_CHI_Mongol_Skirmish.3.d
	picture = GFX_news_MEN_Mongol_Military_Government
	major = yes
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.3.A
		trigger = {
			NOT = { tag = JAP }
		}
	}
	# Send equipment to Mongol Military Government
	option = {
		name = JAP_CHI_Mongol_Skirmish.3.B
		trigger = {
			tag = JAP
		}
		send_equipment = {
			equipment = infantry_equipment
			amount = 5000
			target = MEN
		}
		hidden_effect = {
			country_event = { id = JAP_CHI_Mongol_Skirmish.4 days = 1 }
		}
	}
}
###########################
# China Responds (Japan)
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.4
	title = JAP_CHI_Mongol_Skirmish.4.t
	desc = JAP_CHI_Mongol_Skirmish.4.d
	picture = GFX_report_CHI_Suiyuan_Campaign
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.4.A
		CHI = {
			send_equipment = {
				equipment = infantry_equipment
				amount = 5000
				target = SHX
			}
		}
		hidden_effect = {
			country_event = { id = JAP_CHI_Mongol_Skirmish.5 days = 30 }
		}
	}
}
###########################
# The Suiyuan Campaign (Japan)
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.5
	title = JAP_CHI_Mongol_Skirmish.5.t
	desc = JAP_CHI_Mongol_Skirmish.5.d
	picture = GFX_report_CHI_Suiyuan_Campaign
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.5.A
		hidden_effect = {
			load_oob = SHX_1936
		}
		612 = {
			set_border_war = yes
		}
		827 = {
			set_border_war = yes
		}
		start_border_war = {
			change_state_after_war = no
			attacker = {
				state = 612
				num_provinces = 3
				on_win = JAP_CHI_Mongol_Skirmish.6
				on_lose = JAP_CHI_Mongol_Skirmish.7
			}
			defender = {
				state = 827
				num_provinces = 4
				on_win = JAP_CHI_Mongol_Skirmish.7
				on_lose = JAP_CHI_Mongol_Skirmish.6
			}
		}
		hidden_effect = {
			set_border_war_data = {
				attacker = 612
				defender = 827
				attacker_modifier = -0.5
				defender_modifier = 0.5
			}
		}
	}
}
###########################
# Mongol Military State wins the Skirmish
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.6
	title = JAP_CHI_Mongol_Skirmish.6.t
	desc = JAP_CHI_Mongol_Skirmish.6.d
	picture = GFX_report_MEN_Inner_Mongolian_Army
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.6.A
		612 = { set_border_war = no }
		827 = { set_border_war = no }
		MEN = {
			transfer_state = 827
		}
	}
}
###########################
# Shanxi Clique wins the Skirmish
###########################
country_event = {
	id = JAP_CHI_Mongol_Skirmish.7
	title = JAP_CHI_Mongol_Skirmish.7.t
	desc = JAP_CHI_Mongol_Skirmish.7.d
	picture = GFX_report_CHI_Suiyuan_Campaign
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Mongol_Skirmish.7.A
		612 = { set_border_war = no }
		827 = { set_border_war = no }
		MEN = {
			add_war_support = -0.05
		}
		SHX = {
			add_war_support = 0.05
			add_political_power = 25
		}
	}
}
###########################
# Non-Aggression Pact with the Soviets
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.1
	title = JAP_CHI_Sino_Japanese_War.1.t
	desc = JAP_CHI_Sino_Japanese_War.1.d
	picture = GFX_report_JAP_Soviet_Japanese_Neutrality_Pact
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Sino_Japanese_War.1.A
	}
}
###########################
# Non-Aggression Pact with China
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.2
	title = JAP_CHI_Sino_Japanese_War.2.t
	desc = JAP_CHI_Sino_Japanese_War.2.d
	picture = GFX_report_CHI_Lushan_Declaration
	is_triggered_only = yes
	#
	option = {
		name = JAP_CHI_Sino_Japanese_War.2.A
		CHI = {
			set_country_flag = CHI_NAP_with_Soviets
		}
	}
}
###########################
# The Republic of China asks for Money
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.3
	title = JAP_CHI_Sino_Japanese_War.3.t
	desc = JAP_CHI_Sino_Japanese_War.3.d
	picture = GFX_report_CHI_Lushan_Declaration
	is_triggered_only = yes
	# 500
	option = {
		name = JAP_CHI_Sino_Japanese_War.3.A
		ai_chance = {
			factor = 0
			modifier = {
				set_temp_variable = { amount_of_money_to_check = 500 }
				has_specific_amount_of_money_with_inflation_included = yes
				factor = 60
			}
		}
		set_temp_variable = { money_to_gain = -500 }
		add_money_with_inflation_included_with_tooltip_effect = yes
		CHI = {
			set_temp_variable = { money_to_gain = 500 }
			add_money_with_inflation_included_with_tooltip_effect = yes
		}
	}
	# 600
	option = {
		name = JAP_CHI_Sino_Japanese_War.3.B
		ai_chance = {
			factor = 0
			modifier = {
				set_temp_variable = { amount_of_money_to_check = 600 }
				has_specific_amount_of_money_with_inflation_included = yes
				factor = 20
			}
		}
		set_temp_variable = { money_to_gain = -600 }
		add_money_with_inflation_included_with_tooltip_effect = yes
		CHI = {
			set_temp_variable = { money_to_gain = 600 }
			add_money_with_inflation_included_with_tooltip_effect = yes
		}
	}
	# 700
	option = {
		name = JAP_CHI_Sino_Japanese_War.3.C
		ai_chance = {
			factor = 0
			modifier = {
				set_temp_variable = { amount_of_money_to_check = 700 }
				has_specific_amount_of_money_with_inflation_included = yes
				factor = 10
			}
		}
		set_temp_variable = { money_to_gain = -700 }
		add_money_with_inflation_included_with_tooltip_effect = yes
		CHI = {
			set_temp_variable = { money_to_gain = 700 }
			add_money_with_inflation_included_with_tooltip_effect = yes
		}
	}
	# Nothing
	option = {
		name = JAP_CHI_Sino_Japanese_War.3.D
		ai_chance = {
			factor = 10
		}
	}
}
###########################
# The Marco-Polo Bridge Incident (Japan Wins)
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.4
	title = JAP_CHI_Sino_Japanese_War.4.t
	desc = JAP_CHI_Sino_Japanese_War.4.d
	picture = GFX_report_JAP_Marco_Polo_Incident
	is_triggered_only = yes
	#
	option = {
		trigger = {
			original_tag = JAP
		}
		name = JAP_CHI_Sino_Japanese_War.4.A
		JAP = {
			transfer_state = 608
		}
		hidden_effect = {
			MEN = {
				transfer_state = 611
			}
		}
	}
	option = {
		trigger = {
			original_tag = CHI
		}
		name = JAP_CHI_Sino_Japanese_War.4.B
		JAP = {
			country_event = { id = JAP_CHI_Sino_Japanese_War.5 days = 1 }
		}
	}
}
###########################
# The Chinese Government Demands (Japan Wins)
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.5
	title = JAP_CHI_Sino_Japanese_War.5.t
	desc = JAP_CHI_Sino_Japanese_War.5.d
	picture = GFX_report_CHI_Marco_Polo_Incident
	is_triggered_only = yes
	# We shall accept their demands
	option = {
		name = JAP_CHI_Sino_Japanese_War.5.A
		ai_chance = {
			factor = 0
		}
		add_war_support = -0.1
		set_temp_variable = { money_to_gain = -500 }
		add_money_with_inflation_included_with_tooltip_effect = yes
		CHI = {
			transfer_state = 608
			set_temp_variable = { money_to_gain = 500 }
			add_money_with_inflation_included_with_tooltip_effect = yes
		}
	}
	# The Japanese army will not take such an insult lightly
	option = {
		name = JAP_CHI_Sino_Japanese_War.5.B
		ai_chance = {
			factor = 100
		}
		JAP = {
			declare_war_on = {
				target = CHI
				type = puppet_wargoal_focus
			}
		}
	}
}
###########################
# The Marco-Polo Bridge Incident (China Wins)
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.6
	title = JAP_CHI_Sino_Japanese_War.6.t
	desc = JAP_CHI_Sino_Japanese_War.6.d
	picture = GFX_report_CHI_Marco_Polo_Incident
	is_triggered_only = yes
	# China must pay for this!
	option = {
		trigger = {
			original_tag = JAP
		}
		name = JAP_CHI_Sino_Japanese_War.6.A
		CHI = {
			country_event = { id = JAP_CHI_Sino_Japanese_War.7 days = 1 }
		}
		hidden_effect = {
			MEN = {
				transfer_state = 611
			}
		}
	}
	#
	option = {
		trigger = {
			original_tag = CHI
		}
		name = JAP_CHI_Sino_Japanese_War.6.B
		add_war_support = 0.1
	}
}
###########################
# An Ultimatum (China Wins)
###########################
country_event = {
	id = JAP_CHI_Sino_Japanese_War.7
	title = JAP_CHI_Sino_Japanese_War.7.t
	desc = JAP_CHI_Sino_Japanese_War.7.d
	picture = GFX_report_JAP_Japanese_Korea
	is_triggered_only = yes
	# We have reached the breaking point, never!
	option = {
		name = JAP_CHI_Sino_Japanese_War.7.A
		ai_chance = {
			factor = 100
		}
		hidden_effect = {
			JAP = {
				declare_war_on = {
					target = CHI
					type = puppet_wargoal_focus
				}
			}
		}
	}
	# We cannot afford war with Japan yet
	option = {
		name = JAP_CHI_Sino_Japanese_War.7.B
		ai_chance = {
			factor = 0
		}
		JAP = {
			transfer_state = 608
		}
	}
}
# China Comes Under Further Attack
country_event = {
	id = JAP_CHI_Sino_Japanese_War.8
	title = JAP_CHI_Sino_Japanese_War.8.t
	desc = JAP_CHI_Sino_Japanese_War.8.d
	picture = GFX_report_JAP_Marco_Polo_Incident
	fire_only_once = yes
	trigger = {
		original_tag = CHI
		has_war_with = JAP
		JAP = { has_offensive_war = yes }
	}
	mean_time_to_happen = {
		days = 1
	}
	option = {
		name = JAP_CHI_Sino_Japanese_War.8.A
		create_faction = FACTION_Second_United_Front
		set_global_flag = CHI_Sino_Japanese_War
	}
}
# China Is Under Attack
country_event = {
	id = JAP_CHI_Sino_Japanese_War.9
	title = JAP_CHI_Sino_Japanese_War.9.t
	desc = JAP_CHI_Sino_Japanese_War.9.d
	picture = GFX_report_CHI_100_Regiments_Offensive
	trigger = {
		has_global_flag = CHI_Sino_Japanese_War
		OR = {
			original_tag = SHX
			original_tag = XSM
			original_tag = NEA
			original_tag = SIC
			original_tag = XIK
			original_tag = GND
			original_tag = GXC
			original_tag = YUN
			original_tag = SIK
			original_tag = PRC
		}
		NOT = { has_country_flag = CHI_Joined_Faction }
	}
	mean_time_to_happen = {
		days = 7
	}
	option = {
		name = JAP_CHI_Sino_Japanese_War.9.A
		CHI = {
			add_to_faction = ROOT
		}
		add_ai_strategy = {
			type = alliance
			id = "CHI"
			value = 200
		}
		set_country_flag = CHI_Joined_Faction
	}
}
