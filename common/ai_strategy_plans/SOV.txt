Soviet_historical = {
	name = "Soviet historical plan"
	desc = "Essentially historical behaviour for Soviet"

	enable = {
		original_tag = SOV
		is_historical_focus_on = yes
	}
	abort = {}

	# ai_national_focuses = {
	# 	SOV_finish_five_year_plan
	# 	SOV_production_effort
	# 	SOV_collectivist_propaganda
	# 	#SOV_xian_incident #focus removed
	# 	SOV_stalin_constitution
	# 	SOV_extra_tech_slot_early
	# 	SOV_great_purge
	# 	SOV_improve_railway
	# 	SOV_transpolar_flights
	# 	SOV_ocean_going_navy
	# 	SOV_peoples_commissariat
	# 	SOV_anti_fascist_diplomacy
	# 	SOV_far_east_fortification
	# 	SOV_operation_zet
	# 	SOV_militarized_schools
	# 	SOV_infrastructure_effort
	# 	SOV_workers_culture
	# 	SOV_improve_stalin_line
	# 	SOV_claims_on_baltic
	# 	SOV_tranformation_of_nature
	# 	SOV_socialist_realism
	# 	SOV_baltic_security
	# 	SOV_claims_on_poland
	# 	SOV_demand_eastern_poland
	# 	SOV_anti_capitalist_diplomacy
	# 	SOV_expand_red_fleet
	# 	SOV_claim_bessarabia
	# 	SOV_military_reorganization
	# 	SOV_nkvd_primacy
	# 	SOV_women_pilots
	# 	SOV_research_city_experiment
	# 	SOV_move_industry_to_urals
	# 	SOV_defense_of_moscow
	# 	SOV_lessons_of_war
	# 	SOV_smersh
	# 	SOV_partisan_suppression
	# 	SOV_annex_tannu_tuva
	# 	SOV_war_with_japan
	# }
}