England_historical = {
	name = "England historical plan"
	desc = "Essentially historical behaviour for England"

	enable = {
		original_tag = ENG
		is_historical_focus_on = yes
	}
	abort = {}

	# ai_national_focuses = {
	# 	limited_rearmament_focus
	# 	shadow_scheme_focus
	# 	air_defense_focus
	# 	radar_focus
	# 	uk_industrial_focus
	# 	uk_empire_focus
	# 	uk_service_focus
	# 	general_rearmament_focus
	# 	air_rearmament_focus
	# 	uk_colonial_focus
	# 	uk_asia_focus
	# 	uk_burma_focus
	# 	naval_rearmament_focus
	# 	uk_extra_tech_slot
	# 	ENG_motorized_focus
	# 	ENG_home_defence
	# 	ENG_tank_focus
	# 	crypto_bomb_focus
	# 	ENG_issue_gasmasks
	# 	ENG_military_training_act
	# 	fighter_command_focus
	# 	bomber_command_focus
	# 	coastal_command_focus
	# 	aircraft_production_focus
	# 	tizard_mission_focus
	# 	uk_destroyer_focus
	# 	uk_waves_focus
	# 	uk_convoy_focus
	# 	maud_focus
	# 	uk_mediterranean_focus
	# 	uk_carrier_focus
	# 	hongkong_focus
	# 	uk_iraq_focus
	# 	uk_rock_focus
	# 	singapore_focus
	# 	peninsular_focus
	# 	uk_small_arms_focus
	# 	uk_amphibious_focus
	# 	uk_protect_suez
	# 	uk_malta_focus
	# 	uk_commonwealth_focus
	# 	uk_canada_focus
	# 	uk_australia_focus
	# 	uk_new_zealand_focus
	# 	uk_south_africa_focus
	# 	royal_ordinance_focus
	# 	UK_secret_focus
	# 	uk_jet_focus
	# }
}