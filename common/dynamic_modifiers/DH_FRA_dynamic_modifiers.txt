FRA_Economique_Crisis = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = -0.5
}

FRA_Economique_Crisis_2 = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = -0.3
}

FRA_Economique_Crisis_3 = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = -0.1
}

FRA_Insurrectionary_Centers = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = -0.75
	resistance_growth = 0.25
	resistance_target = 0.25
}

FRA_Colonial_Unrest = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	attrition = 0.2
	state_production_speed_buildings_factor = -0.3
}

FRA_Neo_Destour = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	resistance_growth = 0.2
	state_production_speed_buildings_factor = -0.3
}

FRA_Integrated_Algeria = {
	enable = { always = yes }
	
	icon = GFX_modifiers_sabotaged_resource
	
	required_garrison_factor = -0.5
	resistance_damage_to_garrison = -0.5
	local_resources_factor = 1
	local_non_core_manpower = 0.4
}

FRA_Uncooperative_Italians = {
	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = -0.05
	army_speed_factor = -0.05
}
FRA_Cooperative_Italians = {
	icon = GFX_modifiers_sabotaged_resource
	
	state_production_speed_buildings_factor = 0.05
}