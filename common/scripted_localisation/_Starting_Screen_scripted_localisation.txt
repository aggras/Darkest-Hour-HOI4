defined_text = {
	name = GetStartingScreenTabEntryName
	
	# Country Tab
	text = {
		trigger = {
			check_variable = { v = 1.01 }
		}
		localization_key = STARTING_SCREEN_TAB_HISTORY
	}
	text = {
		trigger = {
			check_variable = { v = 1.02 }
		}
		localization_key = STARTING_SCREEN_TAB_GOALS
	}
	text = {
		trigger = {
			check_variable = { v = 1.03 }
		}
		localization_key = STARTING_SCREEN_TAB_COUNTRY_CONNECTED_MECHANICS
	}
	
	# Mechanics Tab
	text = {
		trigger = {
			check_variable = { v = 2.01 }
		}
		localization_key = STARTING_SCREEN_TAB_MONEY
	}
}

defined_text = {
	name = GetStartingScreenTabEntryText
	
	# Country Tab
	text = {
		trigger = {
			check_variable = { v = 1.01 }
		}
		localization_key = STARTING_SCREEN_TAB_HISTORY_TEXT
	}
	text = {
		trigger = {
			check_variable = { v = 1.02 }
		}
		localization_key = STARTING_SCREEN_TAB_GOALS_TEXT
	}
	text = {
		trigger = {
			check_variable = { v = 1.03 }
		}
		localization_key = STARTING_SCREEN_TAB_COUNTRY_CONNECTED_MECHANICS_TEXT
	}
	
	# Mechanics Tab
	text = {
		trigger = {
			check_variable = { v = 2.01 }
		}
		localization_key = STARTING_SCREEN_TAB_MONEY_TEXT
	}
}

defined_text = {
	name = GetStartingScreenHistoryText
	
	text = {
		trigger = {
			original_tag = GER
			has_start_date < 1933.1.2
		}
		localization_key = GER_1933_desc
	}
	text = {
		trigger = {
			original_tag = ITA
			has_start_date < 1933.1.2
		}
		localization_key = ITA_1933_desc
	}
	text = {
		trigger = {
			original_tag = FRA
			has_start_date < 1933.1.2
		}
		localization_key = FRA_1933_desc
	}
	text = {
		trigger = {
			original_tag = ENG
			has_start_date < 1933.1.2
		}
		localization_key = ENG_1933_desc
	}
	text = {
		trigger = {
			original_tag = SOV
			has_start_date < 1933.1.2
		}
		localization_key = SOV_1933_desc
	}
	text = {
		trigger = {
			original_tag = USA
			has_start_date < 1933.1.2
		}
		localization_key = USA_1933_desc
	}
	text = {
		trigger = {
			original_tag = JAP
			has_start_date < 1933.1.2
		}
		localization_key = JAP_1933_desc
	}
	text = {
		trigger = {
			original_tag = CHI
			has_start_date < 1933.1.2
		}
		localization_key = CHI_1933_desc
	}
	text = {
		trigger = {
			original_tag = POL
			has_start_date < 1933.1.2
		}
		localization_key = POL_1933_desc
	}
	text = {
		trigger = {
			original_tag = ETH
			has_start_date < 1933.1.2
		}
		localization_key = ETH_1933_desc
	}
	text = {
		trigger = {
			always = yes
		}
		localization_key = string_history_text_generic
	}
}

defined_text = {
	name = GetStartingScreenGoalsText
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = string_goals_text_generic
	}
}

defined_text = {
	name = GetStartingScreenConnectedMechanicsText
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = string_connected_mechanics_text_generic
	}
}