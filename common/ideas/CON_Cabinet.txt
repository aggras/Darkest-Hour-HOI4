ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Hendrik J. Elias
		CON_HoG_Hendrik_J_Elias = {
			picture = Hendrik_J_Elias
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hendrik_J_Elias_unavailable }
			}
			
			
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Léonard Mulamba
		CON_HoG_Leonard_Mulamba = {
			picture = Leonard_Mulamba
			allowed = { tag = CON }
			available = {
				date > 1965.1.1
				date < 1971.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Leonard_Mulamba_unavailable }
			}
			
			
			traits = { ideology_A HoG_Political_Protege }
		}
	# Joseph-Désiré Mobutu
		CON_HoG_JosephDesire_Mobutu = {
			picture = JosephDesire_Mobutu
			allowed = { tag = CON }
			available = {
				date > 1966.1.1
				date < 1973.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = JosephDesire_Mobutu_unavailable }
			}
			
			
			traits = { ideology_A HoG_Political_Protege }
		}
	# Mobutu Sese Seko
		CON_HoG_Mobutu_Sese_Seko = {
			picture = Mobutu_Sese_Seko
			allowed = { tag = CON }
			available = {
				date > 1972.1.1
				date < 1998.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mobutu_Sese_Seko_unavailable }
			}
			
			
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Mpinga Kasenda
		CON_HoG_Mpinga_Kasenda = {
			picture = Mpinga_Kasenda
			allowed = { tag = CON }
			available = {
				date > 1977.1.1
				date < 1995.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mpinga_Kasenda_unavailable }
			}
			
			
			traits = { ideology_A HoG_Ambitious_Union_Boss }
		}
	# Bo-Boliko Lokonga
		CON_HoG_BoBoliko_Lokonga = {
			picture = BoBoliko_Lokonga
			allowed = { tag = CON }
			available = {
				date > 1979.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = BoBoliko_Lokonga_unavailable }
			}
			
			
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Jean Nguza Karl-i-Bond
		CON_HoG_Jean_Nguza_KarliBond = {
			picture = Jean_Nguza_KarliBond
			allowed = { tag = CON }
			available = {
				date > 1980.1.1
				date < 2004.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Nguza_KarliBond_unavailable }
			}
			
			
			traits = { ideology_A HoG_Smiling_Oilman }
		}
	# N'singa Udjuu
		CON_HoG_Nsinga_Udjuu = {
			picture = Nsinga_Udjuu
			allowed = { tag = CON }
			available = {
				date > 1981.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nsinga_Udjuu_unavailable }
			}
			
			
			traits = { ideology_A HoG_Ambitious_Union_Boss }
		}
	# Kengo Wa Dondo
		CON_HoG_Kengo_Wa_Dondo = {
			picture = Kengo_Wa_Dondo
			allowed = { tag = CON }
			available = {
				date > 1982.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kengo_Wa_Dondo_unavailable }
			}
			
			
			traits = { ideology_A HoG_Old_Airmarshal }
		}
	# Mabi Mulumba
		CON_HoG_Mabi_Mulumba = {
			picture = Mabi_Mulumba
			allowed = { tag = CON }
			available = {
				date > 1987.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mabi_Mulumba_unavailable }
			}
			
			
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Sambwa Pida Nbagui
		CON_HoG_Sambwa_Pida_Nbagui = {
			picture = Sambwa_Pida_Nbagui
			allowed = { tag = CON }
			available = {
				date > 1988.1.1
				date < 1999.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sambwa_Pida_Nbagui_unavailable }
			}
			
			
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Lunda Bululu
		CON_HoG_Lunda_Bululu = {
			picture = Lunda_Bululu
			allowed = { tag = CON }
			available = {
				date > 1990.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Lunda_Bululu_unavailable }
			}
			
			
			traits = { ideology_A HoG_Old_Admiral }
		}
	# Mulumba Lukoji
		CON_HoG_Mulumba_Lukoji = {
			picture = Mulumba_Lukoji
			allowed = { tag = CON }
			available = {
				date > 1991.1.1
				date < 1998.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mulumba_Lukoji_unavailable }
			}
			
			
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Joseph Iléo
		CON_HoG_Joseph_Ileo = {
			picture = Joseph_Ileo
			allowed = { tag = CON }
			available = {
				date > 1961.1.1
				date < 1995.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Ileo_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Cyrille Adoula
		CON_HoG_Cyrille_Adoula = {
			picture = Cyrille_Adoula
			allowed = { tag = CON }
			available = {
				date > 1962.1.1
				date < 1979.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Cyrille_Adoula_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Moise Tshombe
		CON_HoG_Moise_Tshombe = {
			picture = Moise_Tshombe
			allowed = { tag = CON }
			available = {
				date > 1964.1.1
				date < 1970.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Moise_Tshombe_unavailable }
			}
			
			
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Pierre Ryckmans
		CON_HoG_Pierre_Ryckmans = {
			picture = Pierre_Ryckmans
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Ryckmans_unavailable }
			}
			
			
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Pierre Ryckmans
		CON_HoG_Pierre_Ryckmans_2 = {
			picture = Pierre_Ryckmans_2
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Ryckmans_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Eugene Jungers
		CON_HoG_Eugene_Jungers = {
			picture = Eugene_Jungers
			allowed = { tag = CON }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eugene_Jungers_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Léon Pétillon
		CON_HoG_Leon_Petillon = {
			picture = Leon_Petillon
			allowed = { tag = CON }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Petillon_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Henri Cornelis
		CON_HoG_Henri_Cornelis = {
			picture = Henri_Cornelis
			allowed = { tag = CON }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Cornelis_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Patrice Lumumba
		CON_HoG_Patrice_Lumumba = {
			picture = Patrice_Lumumba
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Patrice_Lumumba_unavailable }
			}
			
			
			traits = { ideology_S HoG_Ambitious_Union_Boss }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Paul Ouwerx
		CON_FM_Paul_Ouwerx = {
			picture = Paul_Ouwerx
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Ouwerx_unavailable }
			}
			
			
			traits = { ideology_A FM_Biased_Intellectual }
		}
	# Eugene Jongers
		CON_FM_Eugene_Jongers = {
			picture = Eugene_Jongers
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eugene_Jongers_unavailable }
			}
			
			
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Moishe Tshombe
		CON_FM_Moishe_Tshombe = {
			picture = Moishe_Tshombe
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Moishe_Tshombe_unavailable }
			}
			
			
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
	# Antoine Gizenga
		CON_FM_Antoine_Gizenga = {
			picture = Antoine_Gizenga
			allowed = { tag = CON }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Antoine_Gizenga_unavailable }
			}
			
			
			traits = { ideology_S FM_Ideological_Crusader }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Albin Nyamoya
		CON_MoS_Albin_Nyamoya = {
			picture = Albin_Nyamoya
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Albin_Nyamoya_unavailable }
			}
			
			
			traits = { ideology_A MoS_Crooked_Kleptocrat }
		}
	# Michel Micombero
		CON_MoS_Michel_Micombero = {
			picture = Michel_Micombero
			allowed = { tag = CON }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Micombero_unavailable }
			}
			
			
			traits = { ideology_A MoS_Back_Stabber }
		}
	# Willem Deruycke
		CON_MoS_Willem_Deruycke = {
			picture = Willem_Deruycke
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Willem_Deruycke_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Charles Ndizeye
		CON_MoS_Charles_Ndizeye = {
			picture = Charles_Ndizeye
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Ndizeye_unavailable }
			}
			
			
			traits = { ideology_S MoS_Compassionate_Gentleman }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Henry M. Flagler
		CON_AM_Henry_M_Flagler = {
			picture = Henry_M_Flagler
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_M_Flagler_unavailable }
			}
			
			
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Baron Gerard Silvercruys
		CON_AM_Baron_Gerard_Silvercruys = {
			picture = Baron_Gerard_Silvercruys
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Baron_Gerard_Silvercruys_unavailable }
			}
			
			
			traits = { ideology_D AM_Administrative_Genius }
		}
	# Gregorie Kayibanda
		CON_AM_Gregorie_Kayibanda = {
			picture = Gregorie_Kayibanda
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Gregorie_Kayibanda_unavailable }
			}
			
			
			traits = { ideology_S AM_Corrupt_Kleptocrat }
		}
	# Josephe Bamina
		CON_AM_Josephe_Bamina = {
			picture = Josephe_Bamina
			allowed = { tag = CON }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Josephe_Bamina_unavailable }
			}
			
			
			traits = { ideology_S AM_Air_To_Ground_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Antoine Lapierre
		CON_HoI_Antoine_Lapierre = {
			picture = Antoine_Lapierre
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Antoine_Lapierre_unavailable }
			}
			
			
			traits = { ideology_A HoI_Dismal_Enigma }
		}
	# Thierry de Vlaminge
		CON_HoI_Thierry_de_Vlaminge = {
			picture = Thierry_de_Vlaminge
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thierry_de_Vlaminge_unavailable }
			}
			
			
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Andre Muhirwa
		CON_HoI_Andre_Muhirwa = {
			picture = Andre_Muhirwa
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Andre_Muhirwa_unavailable }
			}
			
			
			traits = { ideology_S HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Gaston Renondeau
		CON_CoStaff_Gaston_Renondeau = {
			picture = Gaston_Renondeau
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gaston_Renondeau_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Thierry de Vlaminge
		CON_CoStaff_Thierry_de_Vlaminge = {
			picture = Thierry_de_Vlaminge
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thierry_de_Vlaminge_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Josephe Kasavubu
		CON_CoStaff_Josephe_Kasavubu = {
			picture = Josephe_Kasavubu
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Josephe_Kasavubu_unavailable }
			}
			
			
			traits = { ideology_S CoStaff_School_Of_Manoeuvre }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Mathieu LeMagnen
		CON_CoArmy_Mathieu_LeMagnen = {
			picture = Mathieu_LeMagnen
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mathieu_LeMagnen_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Paul van Reynden
		CON_CoArmy_Paul_van_Reynden = {
			picture = Paul_van_Reynden
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_van_Reynden_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Raul Defraiteur
		CON_CoArmy_Raul_Defraiteur = {
			picture = Raul_Defraiteur
			allowed = { tag = CON }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Raul_Defraiteur_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Joseph D. Mobutu
		CON_CoArmy_Joseph_D_Mobutu = {
			picture = Joseph_D_Mobutu
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_D_Mobutu_unavailable }
			}
			
			
			traits = { ideology_S CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Gerrit Berghen
		CON_CoNavy_Gerrit_Berghen = {
			picture = Gerrit_Berghen
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gerrit_Berghen_unavailable }
			}
			
			
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# Dominique Mbonyumutwa
		CON_CoNavy_Dominique_Mbonyumutwa = {
			picture = Dominique_Mbonyumutwa
			allowed = { tag = CON }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Dominique_Mbonyumutwa_unavailable }
			}
			
			
			traits = { ideology_A CoNavy_Power_Projection_Doctrine }
		}
	# Paul van Reynden
		CON_CoNavy_Paul_van_Reynden = {
			picture = Paul_van_Reynden
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_van_Reynden_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Jean Pierre Raffage
		CON_CoNavy_Jean_Pierre_Raffage = {
			picture = Jean_Pierre_Raffage
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Pierre_Raffage_unavailable }
			}
			
			
			traits = { ideology_S CoNavy_Open_Seas_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Gerrit Berghen
		CON_CoAir_Gerrit_Berghen = {
			picture = Gerrit_Berghen
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gerrit_Berghen_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Naval_Aviation_Doctrine }
		}
	# Pierre P. Pascal
		CON_CoAir_Pierre_P_Pascal = {
			picture = Pierre_P_Pascal
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_P_Pascal_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Louis Rwagasore
		CON_CoAir_Louis_Rwagasore = {
			picture = Louis_Rwagasore
			allowed = { tag = CON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_Rwagasore_unavailable }
			}
			
			
			traits = { ideology_S CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
