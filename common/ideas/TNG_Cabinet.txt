ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Ma Hushan
		TNG_HoG_Ma_Hushan = {
			picture = Ma_Hushan
			allowed = { tag  = TNG }
			visible = {
				date > 1900.1.1
				date < 1954.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ma_Hushan_unavailable }
			}
			
			
			traits = { ideology_A POSITION_Deputy_Commander HoG_Local_Tyrant }
		}
	}
}
