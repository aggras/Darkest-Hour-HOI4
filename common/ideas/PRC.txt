ideas = {
#########################################################################
#  National Spirits
#########################################################################
	country = {
		PRC_The_Quarantined_State = {
			picture = PRC_The_Quarantined_State
			available = {
				always = yes
			}
			modifier = {
				industrial_capacity_factory = -0.05
				production_speed_arms_factory_factor = -0.3
				consumer_goods_factor = 0.05
			}
		}
		PRC_The_Quarantined_State_1 = {
			picture = PRC_The_Quarantined_State
			available = {
				always = yes
			}
			modifier = {
				production_speed_arms_factory_factor = -0.25
				consumer_goods_factor = 0.05
			}
		}
		PRC_The_Left_Cannot_Be_Wrong = {
			picture = PRC_The_Left_Cannot_Be_Wrong
			available = {
				always = yes
			}
			modifier = {
				political_power_factor = -0.30
				stability_factor = -0.15
			}
		}
		PRC_Conflicted_Military_1 = {
			picture = PRC_Conflicted_Military_1
			available = {
				always = yes
			}
			modifier = {
				max_dig_in = 1
				dig_in_speed_factor = 0.05
				experience_gain_army_factor = -0.2
			}
		}
		PRC_Conflicted_Military_2 = {
			picture = PRC_Conflicted_Military_2
			available = {
				always = yes
			}
			modifier = {
				max_dig_in = 2
				dig_in_speed_factor = 0.1
				experience_gain_army_factor = -0.5
			}
		}
		PRC_Conflicted_Military_3 = {
			picture = PRC_Conflicted_Military_3
			available = {
				always = yes
			}
			modifier = {
				max_dig_in = 2
				dig_in_speed_factor = 0.1
				experience_gain_army_factor = -0.2
			}
		}
		PRC_Revolution_Of_The_People = {
			picture = PRC_Revolution_Of_The_People
			available = {
				always = yes
			}
			modifier = {
				war_support_factor = 0.5
				command_power_gain = 0.1
				surrender_limit = 0.35
			}
		}
		PRC_Revolution_Of_The_People_1 = {
			picture = PRC_Revolution_Of_The_People
			available = {
				always = yes
			}
			modifier = {
				conscription = 0.1
				war_support_factor = 0.5
				command_power_gain = 0.1
				surrender_limit = 0.35
			}
		}
	}
}