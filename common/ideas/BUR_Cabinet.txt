ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Ba Maw
		BUR_HoG_Ba_Maw = {
			picture = Ba_Maw
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ba_Maw_unavailable }
			}
			
			
			traits = { ideology_F HoG_Flamboyant_Tough_Guy }
		}
	# H.R.H. Sao Edward Yang Kyein Tsai
		BUR_HoG_HRH_Sao_Edward_Yang_Kyein_Tsai = {
			picture = HRH_Sao_Edward_Yang_Kyein_Tsai
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Sao_Edward_Yang_Kyein_Tsai_unavailable }
			}
			
			
			traits = { ideology_A HoG_Political_Protege }
		}
	# H.M. Hso Khan Pha of Yawnghwe
		BUR_HoG_HM_Hso_Khan_Pha_of_Yawnghwe = {
			picture = HM_Hso_Khan_Pha_of_Yawnghwe
			allowed = { tag = BUR }
			available = {
				date > 1964.1.1
				date < 2016.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hso_Khan_Pha_of_Yawnghwe_unavailable }
			}
			
			
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Hubert Elvin Rance
		BUR_HoG_Hubert_Elvin_Rance = {
			picture = Hubert_Elvin_Rance
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hubert_Elvin_Rance_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Aung San
		BUR_HoG_Aung_San = {
			picture = Aung_San
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_San_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# U Nu
		BUR_HoG_U_Nu = {
			picture = U_Nu
			allowed = { tag = BUR }
			available = {
				date > 1948.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = U_Nu_unavailable }
			}
			
			
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Sein Win
		BUR_HoG_Sein_Win = {
			picture = Sein_Win
			allowed = { tag = BUR }
			available = {
				date > 1974.1.1
				date < 1994.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Sein_Win_unavailable }
			}
			
			
			traits = { ideology_S HoG_Ambitious_Union_Boss }
		}
	# Maung Maung Kha
		BUR_HoG_Maung_Maung_Kha = {
			picture = Maung_Maung_Kha
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Maung_Maung_Kha_unavailable }
			}
			
			
			traits = { ideology_S HoG_Old_General }
		}
	# Saw Maung
		BUR_HoG_Saw_Maung = {
			picture = Saw_Maung
			allowed = { tag = BUR }
			available = {
				date > 1988.1.1
				date < 1998.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Saw_Maung_unavailable }
			}
			
			
			traits = { ideology_S HoG_Backroom_Backstabber }
		}
	# Ne Win
		BUR_HoG_Ne_Win = {
			picture = Ne_Win
			allowed = { tag = BUR }
			available = {
				date > 1962.1.1
				date < 2003.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ne_Win_unavailable }
			}
			
			
			traits = { ideology_C HoG_Happy_Amateur }
		}
	# Thakin Soe Buu
		BUR_HoG_Thakin_Soe_Buu = {
			picture = Thakin_Soe_Buu
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thakin_Soe_Buu_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_Admiral }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Galon Saw Oo
		BUR_FM_Galon_Saw_Oo = {
			picture = Galon_Saw_Oo
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Galon_Saw_Oo_unavailable }
			}
			
			
			traits = { ideology_F FM_The_Cloak_N_Dagger_Schemer }
		}
	# Thakin Nu
		BUR_FM_Thakin_Nu = {
			picture = Thakin_Nu
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Thakin_Nu_unavailable }
			}
			
			
			traits = { ideology_A FM_Apologetic_Clerk }
		}
	# H.M. Noan Oo of Lawsawk
		BUR_FM_HM_Noan_Oo_of_Lawsawk = {
			picture = HM_Noan_Oo_of_Lawsawk
			allowed = { tag = BUR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Noan_Oo_of_Lawsawk_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Sir Archibald Cochrane
		BUR_FM_Sir_Archibald_Cochrane = {
			picture = Sir_Archibald_Cochrane
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Archibald_Cochrane_unavailable }
			}
			
			
			traits = { ideology_D FM_General_Staffer }
		}
	# U Thant
		BUR_FM_U_Thant = {
			picture = U_Thant
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = U_Thant_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# San Yu
		BUR_FM_San_Yu = {
			picture = San_Yu
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = San_Yu_unavailable }
			}
			
			
			traits = { ideology_S FM_General_Staffer }
		}
	# Thaik Tun
		BUR_FM_Thaik_Tun = {
			picture = Thaik_Tun
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thaik_Tun_unavailable }
			}
			
			
			traits = { ideology_C FM_Ideological_Crusader }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Ne Win
		BUR_MoS_Ne_Win = {
			picture = Ne_Win
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ne_Win_unavailable }
			}
			
			
			traits = { ideology_F MoS_Compassionate_Gentleman }
		}
	# Sein Lwin
		BUR_MoS_Sein_Lwin = {
			picture = Sein_Lwin
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sein_Lwin_unavailable }
			}
			
			
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# H.R.M. Khun Htun Oo of Hsipaw
		BUR_MoS_HRM_Khun_Htun_Oo_of_Hsipaw = {
			picture = HRM_Khun_Htun_Oo_of_Hsipaw
			allowed = { tag = BUR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRM_Khun_Htun_Oo_of_Hsipaw_unavailable }
			}
			
			
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# John Collins
		BUR_MoS_John_Collins = {
			picture = John_Collins
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_Collins_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Wing Maung
		BUR_MoS_Wing_Maung = {
			picture = Wing_Maung
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wing_Maung_unavailable }
			}
			
			
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Sein Lwin
		BUR_MoS_Sein_Lwin_2 = {
			picture = Sein_Lwin_2
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Sein_Lwin_unavailable }
			}
			
			
			traits = { ideology_S MoS_Efficient_Sociopath }
		}
	# A.N. Ghosal
		BUR_MoS_AN_Ghosal = {
			picture = AN_Ghosal
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = AN_Ghosal_unavailable }
			}
			
			
			traits = { ideology_C MoS_Man_Of_The_People }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Ba Khin
		BUR_AM_Ba_Khin = {
			picture = Ba_Khin
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ba_Khin_unavailable }
			}
			
			
			traits = { ideology_F AM_Air_To_Ground_Proponent }
		}
	# James Franklyn
		BUR_AM_James_Franklyn = {
			picture = James_Franklyn
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = James_Franklyn_unavailable }
			}
			
			
			traits = { ideology_A AM_Administrative_Genius }
		}
	# H.M. Yin Nwe of Kengtung
		BUR_AM_HM_Yin_Nwe_of_Kengtung = {
			picture = HM_Yin_Nwe_of_Kengtung
			allowed = { tag = BUR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Yin_Nwe_of_Kengtung_unavailable }
			}
			
			
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Robert Fitzhendricks
		BUR_AM_Robert_Fitzhendricks = {
			picture = Robert_Fitzhendricks
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Fitzhendricks_unavailable }
			}
			
			
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Aung Gyi
		BUR_AM_Aung_Gyi = {
			picture = Aung_Gyi
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_Gyi_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Tun Tin
		BUR_AM_Tun_Tin = {
			picture = Tun_Tin
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Tun_Tin_unavailable }
			}
			
			
			traits = { ideology_S AM_Corrupt_Kleptocrat }
		}
	# Thakin Ba Thaung
		BUR_AM_Thakin_Ba_Thaung = {
			picture = Thakin_Ba_Thaung
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thakin_Ba_Thaung_unavailable }
			}
			
			
			traits = { ideology_C AM_Submarine_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Keneko Toyoji
		BUR_HoI_Keneko_Toyoji = {
			picture = Keneko_Toyoji
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Keneko_Toyoji_unavailable }
			}
			
			
			traits = { ideology_F HoI_Technical_Specialist }
		}
	# Cecil Crouch Ford
		BUR_HoI_Cecil_Crouch_Ford = {
			picture = Cecil_Crouch_Ford
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Cecil_Crouch_Ford_unavailable }
			}
			
			
			traits = { ideology_A HoI_Political_Specialist }
		}
	# H.R.M. Khun Htun Oo of Hsipaw
		BUR_HoI_HRM_Khun_Htun_Oo_of_Hsipaw = {
			picture = HRM_Khun_Htun_Oo_of_Hsipaw
			allowed = { tag = BUR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRM_Khun_Htun_Oo_of_Hsipaw_unavailable }
			}
			
			
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Lord Ralph Bottomley
		BUR_HoI_Lord_Ralph_Bottomley = {
			picture = Lord_Ralph_Bottomley
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lord_Ralph_Bottomley_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Hla Myaing
		BUR_HoI_Hla_Myaing = {
			picture = Hla_Myaing
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hla_Myaing_unavailable }
			}
			
			
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Khin Nyunt
		BUR_HoI_Khin_Nyunt = {
			picture = Khin_Nyunt
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khin_Nyunt_unavailable }
			}
			
			
			traits = { ideology_S HoI_Political_Specialist }
		}
	# Hem Chandra Mazumdar
		BUR_HoI_Hem_Chandra_Mazumdar = {
			picture = Hem_Chandra_Mazumdar
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Hem_Chandra_Mazumdar_unavailable }
			}
			
			
			traits = { ideology_C HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Khun Sa
		BUR_CoStaff_Khun_Sa = {
			picture = Khun_Sa
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Khun_Sa_unavailable }
			}
			
			
			traits = { ideology_F CoStaff_School_Of_Manoeuvre }
		}
	# Bo Mya
		BUR_CoStaff_Bo_Mya = {
			picture = Bo_Mya
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Bo_Mya_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Aung Gyi
		BUR_CoStaff_Aung_Gyi = {
			picture = Aung_Gyi
			allowed = { tag = BUR }
			available = {
				date > 1964.1.1
				date < 2012.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_Gyi_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Sir Archibald Cochrane
		BUR_CoStaff_Sir_Archibald_Cochrane = {
			picture = Sir_Archibald_Cochrane
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Archibald_Cochrane_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Mason Fitzgerald
		BUR_CoStaff_Mason_Fitzgerald = {
			picture = Mason_Fitzgerald
			allowed = { tag = BUR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mason_Fitzgerald_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Smith Dun
		BUR_CoStaff_Smith_Dun = {
			picture = Smith_Dun
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Smith_Dun_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Kyaw Htin
		BUR_CoStaff_Kyaw_Htin = {
			picture = Kyaw_Htin
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Kyaw_Htin_unavailable }
			}
			
			
			traits = { ideology_S CoStaff_School_Of_Mass_Combat }
		}
	# Ne Win
		BUR_CoStaff_Ne_Win = {
			picture = Ne_Win
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ne_Win_unavailable }
			}
			
			
			traits = { ideology_C CoStaff_School_Of_Manoeuvre }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Ne Win
		BUR_CoArmy_Ne_Win = {
			picture = Ne_Win
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ne_Win_unavailable }
			}
			
			
			traits = { ideology_F CoArmy_Decisive_Battle_Doctrine }
		}
	# Galon Saw Oo
		BUR_CoArmy_Galon_Saw_Oo = {
			picture = Galon_Saw_Oo
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Galon_Saw_Oo_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Elastic_Defence_Doctrine }
		}
	# Aung Gyi
		BUR_CoArmy_Aung_Gyi = {
			picture = Aung_Gyi
			allowed = { tag = BUR }
			available = {
				date > 1964.1.1
				date < 2012.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_Gyi_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Donald Clarkson
		BUR_CoArmy_Donald_Clarkson = {
			picture = Donald_Clarkson
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Donald_Clarkson_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# William Merrick
		BUR_CoArmy_William_Merrick = {
			picture = William_Merrick
			allowed = { tag = BUR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_Merrick_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Aung San
		BUR_CoArmy_Aung_San = {
			picture = Aung_San
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_San_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Saw Maung
		BUR_CoArmy_Saw_Maung = {
			picture = Saw_Maung
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Saw_Maung_unavailable }
			}
			
			
			traits = { ideology_S CoArmy_Guns_And_Butter_Doctrine }
		}
	# Maung Khin
		BUR_CoArmy_Maung_Khin = {
			picture = Maung_Khin
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Maung_Khin_unavailable }
			}
			
			
			traits = { ideology_C CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Hsaya Maung Thaw Ka
		BUR_CoNavy_Hsaya_Maung_Thaw_Ka = {
			picture = Hsaya_Maung_Thaw_Ka
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Hsaya_Maung_Thaw_Ka_unavailable }
			}
			
			
			traits = { ideology_F CoNavy_Open_Seas_Doctrine }
		}
	# Claude Poigdestre
		BUR_CoNavy_Claude_Poigdestre = {
			picture = Claude_Poigdestre
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Claude_Poigdestre_unavailable }
			}
			
			
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Aung Gyi
		BUR_CoNavy_Aung_Gyi = {
			picture = Aung_Gyi
			allowed = { tag = BUR }
			available = {
				date > 1964.1.1
				date < 2012.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_Gyi_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Hubert Scott-Paine
		BUR_CoNavy_Hubert_ScottPaine = {
			picture = Hubert_ScottPaine
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hubert_ScottPaine_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Khin Maung Swe
		BUR_CoNavy_Khin_Maung_Swe = {
			picture = Khin_Maung_Swe
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khin_Maung_Swe_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Thein Nyunt
		BUR_CoNavy_Thein_Nyunt = {
			picture = Thein_Nyunt
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Thein_Nyunt_unavailable }
			}
			
			
			traits = { ideology_S CoNavy_Indirect_Approach_Doctrine }
		}
	# Thakin Soe Buu
		BUR_CoNavy_Thakin_Soe_Buu = {
			picture = Thakin_Soe_Buu
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thakin_Soe_Buu_unavailable }
			}
			
			
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Tin Tun
		BUR_CoAir_Tin_Tun = {
			picture = Tin_Tun
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Tin_Tun_unavailable }
			}
			
			
			traits = { ideology_F CoAir_Carpet_Bombing_Doctrine }
		}
	# Aik Luk
		BUR_CoAir_Aik_Luk = {
			picture = Aik_Luk
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Aik_Luk_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Air_Superiority_Doctrine }
		}
	# Aung Gyi
		BUR_CoAir_Aung_Gyi = {
			picture = Aung_Gyi
			allowed = { tag = BUR }
			available = {
				date > 1964.1.1
				date < 2012.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aung_Gyi_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# P.C. Maltby
		BUR_CoAir_PC_Maltby = {
			picture = PC_Maltby
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = PC_Maltby_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Naw Seng
		BUR_CoAir_Naw_Seng = {
			picture = Naw_Seng
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Naw_Seng_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Thein Win
		BUR_CoAir_Thein_Win = {
			picture = Thein_Win
			allowed = { tag = BUR }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Thein_Win_unavailable }
			}
			
			
			traits = { ideology_S CoAir_Army_Aviation_Doctrine }
		}
	# Thaik Tun
		BUR_CoAir_Thaik_Tun = {
			picture = Thaik_Tun
			allowed = { tag = BUR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thaik_Tun_unavailable }
			}
			
			
			traits = { ideology_C CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
