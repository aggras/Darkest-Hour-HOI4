####################################################
## Hungary
#####################################################

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

# Hungary declares war to Carpatho-Ukraine
Hungary_war_with_Carpatho_Ukraine = {
	enable = {
		original_tag = HUN
		country_exists = CPU
	}
	abort = {
		NOT = { country_exists = CPU }
	}

	ai_strategy = {
		type = invade
		id = "CPU"
		value = 200
	}
	ai_strategy = {
		type = conquer
		id = "CPU"
		value = 200
	}
}