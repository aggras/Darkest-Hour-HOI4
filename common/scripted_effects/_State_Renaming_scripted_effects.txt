State_Renaming_Base = {
	if = {
        limit = {
            ROOT = { has_country_flag = language_hebrew }
        }
        State_Renaming_Hebrew = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_basque }
        }
        State_Renaming_Basque = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_czech }
        }
        State_Renaming_Czech = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_bulgarian }
        }
        State_Renaming_Bulgarian = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_hungarian }
        }
        State_Renaming_Hungarian = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_greenlandic }
        }
        State_Renaming_Greenlandic = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_german }
        }
        State_Renaming_German = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_lithuanian }
        }
        State_Renaming_Lithuanian = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_catalan }
        }
        State_Renaming_Catalan = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_italian }
        }
        State_Renaming_Italian = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_danish }
        }
        State_Renaming_Danish = yes
    }
    if = {
        limit = {
            ROOT = { has_country_flag = language_faroese }
        }
        State_Renaming_Faroese = yes
    }
}

State_Renaming_Hebrew = {
    if = {
        limit = {
            FROM.FROM = { state = 822 }
        }
        FROM.FROM = { 
            set_province_name = {
				id = 1015
				name = VICTORY_POINTS_1015_hebrew
			}
            set_province_name = {
				id = 13212
				name = VICTORY_POINTS_13212_hebrew
			}
        }
    }
	if = {
        limit = {
            FROM.FROM = { state = 823 }
        }
        FROM.FROM = { 
            set_state_name = STATE_823_hebrew
            set_province_name = {
				id = 4206
				name = VICTORY_POINTS_4206_hebrew
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1612 }
        }
        FROM.FROM = { 
            set_state_name = STATE_1612_hebrew
        }
    }
}

State_Renaming_Basque = {
    if = {
        limit = {
            FROM.FROM = { state = 172 }
        }
        FROM.FROM = {
            set_state_name = STATE_172_basque
            set_province_name = {
				id = 3933
				name = VICTORY_POINTS_3933_basque
			}
        }
    }
	if = {
        limit = {
            FROM.FROM = { state = 776 }
        }
        FROM.FROM = { 
            set_state_name = STATE_776_basque
            set_province_name = {
				id = 740
				name = VICTORY_POINTS_740_basque
			}
            set_province_name = {
				id = 3735
				name = VICTORY_POINTS_3735_basque
			}
            set_province_name = {
				id = 6756
				name = VICTORY_POINTS_6756_basque
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1637 }
        }
        FROM.FROM = {
            set_state_name = STATE_1637_basque
            set_province_name = {
				id = 743
				name = VICTORY_POINTS_743_basque
			}
        }
    }
}

State_Renaming_Czech = {
    if = {
        limit = {
            FROM.FROM = { state = 1120 }
        }
        FROM.FROM = {
            set_state_name = STATE_1120_czech
            set_province_name = {
				id = 3485
				name = VICTORY_POINTS_3485_czech
			}
        }
    }
}

State_Renaming_Bulgarian = {
    if = {
        limit = {
            FROM.FROM = { state = 77 }
        }
        FROM.FROM = {
            set_state_name = STATE_77_bulgarian
            set_province_name = {
				id = 11597
				name = VICTORY_POINTS_11597_bulgarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 834 }
        }
        FROM.FROM = {
            set_state_name = STATE_834_bulgarian
            set_province_name = {
				id = 9635
				name = VICTORY_POINTS_9635_bulgarian
			}
            set_province_name = {
				id = 687
				name = VICTORY_POINTS_687_bulgarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1263 }
        }
        FROM.FROM = {
            set_state_name = STATE_1263_bulgarian
            set_province_name = {
				id = 11842
				name = VICTORY_POINTS_11842_bulgarian
			}
            set_province_name = {
				id = 922
				name = VICTORY_POINTS_922_bulgarian
			}
        }
    }
}

State_Renaming_Hungarian = {
    if = {
        limit = {
            FROM.FROM = { state = 973 }
        }
        FROM.FROM = {
            set_state_name = STATE_973_hungarian
            set_province_name = {
				id = 9661
				name = VICTORY_POINTS_9661_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 70 }
        }
        FROM.FROM = {
            set_state_name = STATE_70_hungarian
            set_province_name = {
				id = 9692
				name = VICTORY_POINTS_9692_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 664 }
        }
        FROM.FROM = {
            set_state_name = STATE_664_hungarian
            set_province_name = {
				id = 6573
				name = VICTORY_POINTS_6573_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1383 }
        }
        FROM.FROM = {
            set_state_name = STATE_1383_hungarian
            set_province_name = {
				id = 14486
				name = VICTORY_POINTS_14486_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1107 }
        }
        FROM.FROM = {
            set_state_name = STATE_1107_hungarian
            set_province_name = {
				id = 11539
				name = VICTORY_POINTS_11539_hungarian
			}
            set_province_name = {
				id = 3537
				name = VICTORY_POINTS_3537_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 71 }
        }
        FROM.FROM = {
            set_state_name = STATE_71_hungarian
            set_province_name = {
				id = 3581
				name = VICTORY_POINTS_3581_hungarian
			}
            set_province_name = {
				id = 3550
				name = VICTORY_POINTS_3550_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1108 }
        }
        FROM.FROM = {
            set_state_name = STATE_1108_hungarian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1106 }
        }
        FROM.FROM = {
            set_state_name = STATE_1106_hungarian
            set_province_name = {
				id = 13634
				name = VICTORY_POINTS_13634_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 73 }
        }
        FROM.FROM = {
            set_state_name = STATE_73_hungarian
            set_province_name = {
				id = 3548
				name = VICTORY_POINTS_3548_hungarian
			}
            set_province_name = {
				id = 11691
				name = VICTORY_POINTS_11691_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 839 }
        }
        FROM.FROM = {
            set_state_name = STATE_839_hungarian
            set_province_name = {
				id = 746
				name = VICTORY_POINTS_746_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 76 }
        }
        FROM.FROM = {
            set_state_name = STATE_76_hungarian
            set_province_name = {
				id = 3696
				name = VICTORY_POINTS_3696_hungarian
			}
            set_province_name = {
				id = 6711
				name = VICTORY_POINTS_6711_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 838 }
        }
        FROM.FROM = {
            set_state_name = STATE_838_hungarian
            set_province_name = {
				id = 9670
				name = VICTORY_POINTS_9670_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 84 }
        }
        FROM.FROM = {
            set_state_name = STATE_84_hungarian
            set_province_name = {
				id = 6679
				name = VICTORY_POINTS_6679_hungarian
			}
            set_province_name = {
				id = 690
				name = VICTORY_POINTS_690_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 83 }
        }
        FROM.FROM = {
            set_state_name = STATE_83_2
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 82 }
        }
        FROM.FROM = {
            set_state_name = STATE_82_hungarian
            set_province_name = {
				id = 9606
				name = VICTORY_POINTS_9606_hungarian
			}
            set_province_name = {
				id = 11592
				name = VICTORY_POINTS_11592_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 772 }
        }
        FROM.FROM = {
            set_state_name = STATE_772_hungarian
            set_province_name = {
				id = 11787
				name = VICTORY_POINTS_11787_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 45 }
        }
        FROM.FROM = {
            set_state_name = STATE_45_hungarian
            set_province_name = {
				id = 9621
				name = VICTORY_POINTS_9621_hungarian
			}
            set_province_name = {
				id = 3617
				name = VICTORY_POINTS_3617_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 978 }
        }
        FROM.FROM = {
            set_state_name = STATE_978_hungarian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 819 }
        }
        FROM.FROM = {
            set_state_name = STATE_819_hungarian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1452 }
        }
        FROM.FROM = {
            set_state_name = STATE_1452_hungarian
            set_province_name = {
				id = 14672
				name = VICTORY_POINTS_14672_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1084 }
        }
        FROM.FROM = {
            set_state_name = STATE_1084_hungarian
            set_province_name = {
				id = 6647
				name = VICTORY_POINTS_6647_hungarian
			}
            set_province_name = {
				id = 9595
				name = VICTORY_POINTS_9595_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 109 }
        }
        FROM.FROM = {
            set_state_name = STATE_109_hungarian
            set_province_name = {
				id = 11581
				name = VICTORY_POINTS_11581_hungarian
			}
            set_province_name = {
				id = 3592
				name = VICTORY_POINTS_3592_hungarian
			}
            set_province_name = {
				id = 3596
				name = VICTORY_POINTS_3596_hungarian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1404 }
        }
        FROM.FROM = {
            set_state_name = STATE_1404_hungarian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 770 }
        }
        FROM.FROM = {
            set_state_name = STATE_770_hungarian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1384 }
        }
        FROM.FROM = {
            set_state_name = STATE_1384_hungarian
        }
    }
}

State_Renaming_Greenlandic = {
    if = {
        limit = {
            FROM.FROM = { state = 101 }
        }
        FROM.FROM = {
            set_state_name = STATE_101_greenlandic
            set_province_name = {
				id = 10245
				name = VICTORY_POINTS_10245_greenlandic
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 841 }
        }
        FROM.FROM = {
            set_state_name = STATE_841_greenlandic
        }
    }
}

State_Renaming_German = {
    if = {
        limit = {
            FROM.FROM = { state = 1169 }
        }
        FROM.FROM = {
            set_state_name = STATE_1169_german_exonym
            set_province_name = {
				id = 11502
				name = VICTORY_POINTS_11502_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 28 }
        }
        FROM.FROM = {
            set_state_name = STATE_28_german
            set_province_name = {
				id = 549
				name = VICTORY_POINTS_549_german
			}
            set_province_name = {
				id = 678
				name = VICTORY_POINTS_678_german
			}
            set_province_name = {
				id = 9503
				name = VICTORY_POINTS_9503_german
			}
            set_province_name = {
				id = 6529
				name = VICTORY_POINTS_6529_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 8 }
        }
        FROM.FROM = {
            set_state_name = STATE_8_german
            set_province_name = {
				id = 6583
				name = VICTORY_POINTS_6583_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 39 }
        }
        FROM.FROM = {
            set_state_name = STATE_39_german
            set_province_name = {
				id = 11598
				name = VICTORY_POINTS_11598_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 769 }
        }
        FROM.FROM = {
            set_state_name = STATE_769_german
            set_province_name = {
				id = 9598
				name = VICTORY_POINTS_9598_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 102 }
        }
        FROM.FROM = {
            set_state_name = STATE_102_german
            set_province_name = {
				id = 3654
				name = VICTORY_POINTS_3654_german
			}
            set_province_name = {
				id = 665
				name = VICTORY_POINTS_665_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 804 }
        }
        FROM.FROM = {
            set_state_name = STATE_804_german
            set_province_name = {
				id = 11251
				name = VICTORY_POINTS_11251_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 188 }
        }
        FROM.FROM = {
            set_state_name = STATE_188_german
            set_province_name = {
				id = 3288
				name = VICTORY_POINTS_3288_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1105 }
        }
        FROM.FROM = {
            set_state_name = STATE_1105_german
            set_province_name = {
				id = 557
				name = VICTORY_POINTS_557_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 69 }
        }
        FROM.FROM = {
            set_state_name = STATE_69_german_exonym
            set_province_name = {
				id = 6470
				name = VICTORY_POINTS_6470_german
			}
            set_province_name = {
				id = 11432
				name = VICTORY_POINTS_11432_german
			}
            set_province_name = {
				id = 9414
				name = VICTORY_POINTS_9414_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 74 }
        }
        FROM.FROM = {
            set_state_name = STATE_74_german
            set_province_name = {
				id = 6562
				name = VICTORY_POINTS_6562_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 798 }
        }
        FROM.FROM = {
            set_state_name = STATE_798_german
            set_province_name = {
				id = 3295
				name = VICTORY_POINTS_3295_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 798 }
            ROOT = { 
                original_tag = GER
                NOT = { has_government = fascist }
            }
        }
        FROM.FROM = {
            set_province_name = {
				id = 9263
				name = VICTORY_POINTS_9263_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 798 }
            ROOT = { 
                original_tag = GER
                has_government = fascist
            }
        }
        FROM.FROM = {
            set_province_name = {
				id = 9263
				name = VICTORY_POINTS_9263_german_2
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 86 }
        }
        FROM.FROM = {
            set_state_name = STATE_86_german
            set_province_name = {
				id = 6558
				name = VICTORY_POINTS_6558_german
			}
            set_province_name = {
				id = 3460
				name = VICTORY_POINTS_3460_german
			}
            set_province_name = {
				id = 3381
				name = VICTORY_POINTS_3381_german
			}
            set_province_name = {
				id = 279
				name = VICTORY_POINTS_279_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 9 }
        }
        FROM.FROM = {
            set_state_name = STATE_9_german
            set_province_name = {
				id = 11542
				name = VICTORY_POINTS_11542_german
			}
            set_province_name = {
				id = 14679
				name = VICTORY_POINTS_14679_german
			}
            set_province_name = {
				id = 6418
				name = VICTORY_POINTS_6418_german
			}
            set_province_name = {
				id = 583
				name = VICTORY_POINTS_583_german
			}
            set_province_name = {
				id = 9569
				name = VICTORY_POINTS_9569_german
			}
            set_province_name = {
				id = 13657
				name = VICTORY_POINTS_13657_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 75 }
        }
        FROM.FROM = {
            set_state_name = STATE_75_german
            set_province_name = {
				id = 3569
				name = VICTORY_POINTS_3569_german
			}
            set_province_name = {
				id = 3553
				name = VICTORY_POINTS_3553_german
			}
            set_province_name = {
				id = 6576
				name = VICTORY_POINTS_6576_german
			}
            set_province_name = {
				id = 6590
				name = VICTORY_POINTS_6590_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 72 }
        }
        FROM.FROM = {
            set_state_name = STATE_72_german
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 766 }
        }
        FROM.FROM = {
            set_state_name = STATE_766_german
            set_province_name = {
				id = 6464
				name = VICTORY_POINTS_6464_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 752 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 3898
				name = VICTORY_POINTS_3898_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1553 }
        }
        FROM.FROM = {
            set_state_name = STATE_1553_german
            set_province_name = {
				id = 11407
				name = VICTORY_POINTS_11407_german
			}
            set_province_name = {
				id = 9418
				name = VICTORY_POINTS_9418_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1204 }
        }
        FROM.FROM = {
            set_state_name = STATE_1204_german
            set_province_name = {
				id = 6537
				name = VICTORY_POINTS_6537_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 847 }
        }
        FROM.FROM = {
            set_state_name = STATE_847_german
            set_province_name = {
				id = 11516
				name = VICTORY_POINTS_11516_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 17 }
        }
        FROM.FROM = {
            set_state_name = STATE_17_german
            set_province_name = {
				id = 9642
				name = VICTORY_POINTS_9642_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 763 }
        }
        FROM.FROM = {
            set_state_name = STATE_763_german
            set_province_name = {
				id = 6545
				name = VICTORY_POINTS_6545_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 34 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 9536
				name = VICTORY_POINTS_9536_german_dutch
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 6 }
        }
        FROM.FROM = {
            set_state_name = STATE_6_german
            set_province_name = {
				id = 516
				name = VICTORY_POINTS_516_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 964 }
        }
        FROM.FROM = {
            set_state_name = STATE_964_german
            set_province_name = {
				id = 6446
				name = VICTORY_POINTS_6446_german
			}
            set_province_name = {
				id = 13266
				name = VICTORY_POINTS_13266_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 35 }
        }
        FROM.FROM = {
            set_state_name = STATE_35_german
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 960 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 11456
				name = VICTORY_POINTS_11456_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 7 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 3314
				name = VICTORY_POINTS_3314_german
			}
            set_province_name = {
				id = 3211
				name = VICTORY_POINTS_3211_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1039 }
        }
        FROM.FROM = {
            set_state_name = STATE_1039_german
            set_province_name = {
				id = 3612
				name = VICTORY_POINTS_3612_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 3 }
        }
        FROM.FROM = {
            set_state_name = STATE_3_german
            set_province_name = {
				id = 9622
				name = VICTORY_POINTS_9622_german
			}
            set_province_name = {
				id = 6666
				name = VICTORY_POINTS_6666_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 767 }
        }
        FROM.FROM = {
            set_state_name = STATE_767_german
            set_province_name = {
				id = 9600
				name = VICTORY_POINTS_9600_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1505 }
        }
        FROM.FROM = {
            set_state_name = STATE_1505_german
            set_province_name = {
				id = 575
				name = VICTORY_POINTS_575_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 762 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 6504
				name = VICTORY_POINTS_6504_german_dutch
			}
            set_province_name = {
				id = 11548
				name = VICTORY_POINTS_11548_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 765 }
        }
        FROM.FROM = {
            set_state_name = STATE_765_german
            set_province_name = {
				id = 9627
				name = VICTORY_POINTS_9627_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 87 }
            ROOT = { 
                original_tag = GER
                NOT = { has_government = fascist }
            }
        }
        FROM.FROM = {
            set_state_name = STATE_87_german
            set_province_name = {
				id = 9508
				name = VICTORY_POINTS_9508_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 87 }
            ROOT = { 
                original_tag = GER
                has_government = fascist
            }
        }
        FROM.FROM = {
            set_state_name = STATE_87_german_2
            set_province_name = {
				id = 9508
				name = VICTORY_POINTS_9508_german_2
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 87 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 9439
				name = VICTORY_POINTS_9508_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 98 }
        }
        FROM.FROM = {
            set_state_name = STATE_98_german
            set_province_name = {
				id = 6321
				name = VICTORY_POINTS_6321_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 98 }
            ROOT = { 
                original_tag = GER
                has_government = fascist
            }
        }
        FROM.FROM = {
            set_province_name = {
				id = 3230
				name = VICTORY_POINTS_3230_german_2
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 10 }
        }
        FROM.FROM = {
            set_state_name = STATE_10_german
            set_province_name = {
				id = 3544
				name = VICTORY_POINTS_3544_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1174 }
        }
        FROM.FROM = {
            set_state_name = STATE_1174_german
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1175 }
        }
        FROM.FROM = {
            set_state_name = STATE_1175_german
            set_province_name = {
				id = 13219
				name = VICTORY_POINTS_13219_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1173 }
        }
        FROM.FROM = {
            set_state_name = STATE_1173_german
            set_province_name = {
				id = 11385
				name = VICTORY_POINTS_11385_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1182 }
        }
        FROM.FROM = {
            set_state_name = STATE_1182
            set_province_name = {
				id = 11366
				name = VICTORY_POINTS_11366
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 814 }
        }
        FROM.FROM = {
            set_state_name = STATE_814_german
            set_province_name = {
				id = 599
				name = VICTORY_POINTS_599_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 99 }
        }
        FROM.FROM = {
            set_state_name = STATE_99_german
            set_province_name = {
				id = 399
				name = VICTORY_POINTS_399_german
			}
            set_province_name = {
				id = 6235
				name = VICTORY_POINTS_6235_german
			}
            set_province_name = {
				id = 6364
				name = VICTORY_POINTS_6364_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1082 }
        }
        FROM.FROM = {
            set_state_name = STATE_1082_german
            set_province_name = {
				id = 3325
				name = VICTORY_POINTS_3325_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 37 }
        }
        FROM.FROM = {
            set_state_name = STATE_37_german
            set_province_name = {
				id = 6287
				name = VICTORY_POINTS_6287_german
			}
            set_province_name = {
				id = 14841
				name = VICTORY_POINTS_14841_german_swedish
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 90 }
        }
        FROM.FROM = {
            set_state_name = STATE_90_german
            set_province_name = {
				id = 6416
				name = VICTORY_POINTS_6416_german
			}
            set_province_name = {
				id = 3586
				name = VICTORY_POINTS_3586_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 88 }
        }
        FROM.FROM = {
            set_state_name = STATE_88_german
            set_province_name = {
				id = 9427
				name = VICTORY_POINTS_9427_german
			}
            set_province_name = {
				id = 417
				name = VICTORY_POINTS_417_german
			}
            set_province_name = {
				id = 6522
				name = VICTORY_POINTS_6522_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 886 }
        }
        FROM.FROM = {
            set_state_name = STATE_886_german
            set_province_name = {
				id = 9494
				name = VICTORY_POINTS_9494_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 89 }
        }
        FROM.FROM = {
            set_state_name = STATE_89_german
            set_province_name = {
				id = 11411
				name = VICTORY_POINTS_11411_german
			}
            set_province_name = {
				id = 6431
				name = VICTORY_POINTS_6431_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 91 }
        }
        FROM.FROM = {
            set_state_name = STATE_91_german
            set_province_name = {
				id = 11479
				name = VICTORY_POINTS_11479_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1172 }
        }
        FROM.FROM = {
            set_state_name = STATE_1172_german
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1426 }
        }
        FROM.FROM = {
            set_state_name = STATE_1426_german
            set_province_name = {
				id = 11274
				name = VICTORY_POINTS_11274_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 810 }
            ROOT = { 
                is_Nazi_Germany = no
            }
        }
        FROM.FROM = {
            set_state_name = STATE_810_german
            set_province_name = {
				id = 3254
				name = VICTORY_POINTS_3254_german
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 810 }
            ROOT = { 
                is_Nazi_Germany = yes
            }
        }
        FROM.FROM = {
            set_state_name = STATE_810_german_2
            set_province_name = {
				id = 3254
				name = VICTORY_POINTS_3254_german_2
			}
        }
    }
}

State_Renaming_Lithuanian = {
    if = {
        limit = {
            FROM.FROM = { state = 810 }
        }
        FROM.FROM = {
            set_state_name = STATE_810_lithuanian
            set_province_name = {
				id = 3254
				name = VICTORY_POINTS_3254_lithuanian
			}
        }
    }
	if = {
        limit = {
            FROM.FROM = { state = 188 }
        }
        FROM.FROM = { 
            set_state_name = STATE_188
            set_province_name = {
				id = 3288
				name = VICTORY_POINTS_3288
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 805 }
        }
        FROM.FROM = { 
            set_state_name = STATE_805_lithuanian
            set_province_name = {
				id = 3320
				name = VICTORY_POINTS_3320_lithuanian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1428 }
        }
        FROM.FROM = { 
            set_state_name = STATE_1428_lithuanian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1171 }
        }
        FROM.FROM = { 
            set_state_name = STATE_1171_lithuanian
        }
    }
}

State_Renaming_Catalan = {
    if = {
        limit = {
            FROM.FROM = { state = 165 }
        }
        FROM.FROM = {
            set_state_name = STATE_165_catalan
            set_province_name = {
				id = 6927
				name = VICTORY_POINTS_6927_catalan
			}
            set_province_name = {
				id = 872
				name = VICTORY_POINTS_872_catalan
			}
        }
    }
	if = {
        limit = {
            FROM.FROM = { state = 1636 }
        }
        FROM.FROM = { 
            set_state_name = STATE_1636_catalan
            set_province_name = {
				id = 9853
				name = VICTORY_POINTS_9853_catalan
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 752 }
        }
        FROM.FROM = { 
            set_state_name = STATE_752
            set_province_name = {
				id = 3898
				name = VICTORY_POINTS_3898
			}
        }
    }
}

State_Renaming_Italian = {
    if = {
        limit = {
            FROM.FROM = { state = 769 }
        }
        FROM.FROM = {
            set_state_name = STATE_769
            set_province_name = {
				id = 9598
				name = VICTORY_POINTS_9598
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 39 }
        }
        FROM.FROM = {
            set_state_name = STATE_39
            set_province_name = {
				id = 11598
				name = VICTORY_POINTS_11598
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 846 }
        }
        FROM.FROM = {
            set_state_name = STATE_846_italian
            set_province_name = {
				id = 9909
				name = VICTORY_POINTS_9909_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 764 }
        }
        FROM.FROM = {
            set_state_name = STATE_764_italian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 735 }
        }
        FROM.FROM = {
            set_state_name = STATE_735_italian
            set_province_name = {
				id = 11573
				name = VICTORY_POINTS_11573_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1 }
        }
        FROM.FROM = {
            set_state_name = STATE_1_italian
            set_province_name = {
				id = 3838
				name = VICTORY_POINTS_3838_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 765 }
        }
        FROM.FROM = {
            set_state_name = STATE_765_italian
            set_province_name = {
				id = 9627
				name = VICTORY_POINTS_9627_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1404 }
        }
        FROM.FROM = {
            set_state_name = STATE_1404_italian
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 116 }
        }
        FROM.FROM = {
            set_province_name = {
				id = 12003
				name = VICTORY_POINTS_12003_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1085 }
        }
        FROM.FROM = {
            set_state_name = STATE_1085_italian
            set_province_name = {
				id = 3924
				name = VICTORY_POINTS_3924_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 103 }
        }
        FROM.FROM = {
            set_state_name = STATE_103_italian
            set_province_name = {
				id = 6889
				name = VICTORY_POINTS_6889_italian
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1091 }
        }
        FROM.FROM = {
            set_state_name = STATE_1091_italian
            set_province_name = {
				id = 13597
				name = VICTORY_POINTS_13597_italian
			}
        }
    }
}

State_Renaming_Danish = {
    if = {
        limit = {
            FROM.FROM = { state = 1182 }
        }
        FROM.FROM = {
            set_state_name = STATE_1182_danish
            set_province_name = {
				id = 11366
				name = VICTORY_POINTS_11366_danish
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 337 }
        }
        FROM.FROM = {
            set_state_name = STATE_337
            set_province_name = {
				id = 13003
				name = VICTORY_POINTS_13003
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 804 }
        }
        FROM.FROM = {
            set_state_name = STATE_804
            set_province_name = {
				id = 11251
				name = VICTORY_POINTS_11251
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 99 }
        }
        FROM.FROM = {
            set_state_name = STATE_99
            set_province_name = {
				id = 399
				name = VICTORY_POINTS_399
			}
            set_province_name = {
				id = 6235
				name = VICTORY_POINTS_6235
			}
            set_province_name = {
				id = 6364
				name = VICTORY_POINTS_6364
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 1082 }
        }
        FROM.FROM = {
            set_state_name = STATE_1082
            set_province_name = {
				id = 3325
				name = VICTORY_POINTS_3325
			}
        }
    }
    if = {
        limit = {
            FROM.FROM = { state = 37 }
        }
        FROM.FROM = {
            set_state_name = STATE_37
            set_province_name = {
				id = 6287
				name = VICTORY_POINTS_6287
			}
            set_province_name = {
				id = 14841
				name = VICTORY_POINTS_14841
			}
        }
    }
}

State_Renaming_Faroese = {
    if = {
        limit = {
            FROM.FROM = { state = 337 }
        }
        FROM.FROM = {
            set_state_name = STATE_337_faroese
            set_province_name = {
				id = 13003
				name = VICTORY_POINTS_13003_faroese
			}
        }
    }
}